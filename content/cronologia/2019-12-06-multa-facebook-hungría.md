---
headless: true
resumen: Multa Facebook Hungría
fecha: 2019-12-06
temas:
  - privacidad
  - sanciones
sujetos:
  - facebook
---

La Autoridad de la Competencia Húngara (GVH) multa a **Facebook** con 1&thinsp;200 millones de forintos (4 millones de US$) por anunciar sus servicios como gratuitos cuando los usuarios no tienen que pagar con dinero pero pagan de facto al generar un beneficio económico a través de su actividad y la recopilación de sus datos. [Competition Policy International](https://www.competitionpolicyinternational.com/hungary-competition-watchdog-fines-facebook/) &middot; [Reuters](https://www.reuters.com/article/us-facebook-hungary-fine-idUSKBN1YA24X)
