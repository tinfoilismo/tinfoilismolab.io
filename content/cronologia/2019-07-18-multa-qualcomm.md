---
headless: true
resumen: Multa Qualcomm
fecha: 2019-07-18
temas:
  - sanciones
sujetos:
  - qualcomm
---

La Comisión Europea multa al fabricante de chips **Qualcomm** con 242 millones de € por vender por debajo del coste de producción para hundir a su rival Icera. [Comisión Europea](https://ec.europa.eu/commission/presscorner/detail/en/ip_19_4350)
