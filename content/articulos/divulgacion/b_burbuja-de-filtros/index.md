---
title: Burbuja de filtros
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/31
draft: true
---

![Sepultura del artista Wolf Vostell (14 de octubre de 1932-3 de abril de 1998) con el epitafio: "Son las cosas que no conocéis las que cambiarán vuestras vidas." en español y hebreo](wolf-vostell.jpg "Epitafio de Wolf Vostell en el cementerio civil de La Almudena.")

A medida que las empresas web adaptan sus servicios a nuestros gustos personales (incluyendo noticias y resultados de búsqueda), como efecto colateral quedamos atrapados en una _burbuja de filtros_ y no nos exponemos a información que pueda desafiar o ampliar nuestra visión del mundo.

Los algoritmos tienden a la convergencia de patrones e intentan, por defecto, simplificar nuestra complejidad. Nos interpretan y nos incluyen en un patrón determinado, lo que atenta directamente contra nuestra individualidad. http://lab.cccb.org/es/hacia-una-nueva-etica-informativa/

Caso de estudio: Discriminación invisible y pobreza (Privacy International). https://privacyinternational.org/case-studies/737/case-study-invisible-discrimination-and-poverty

El algoritmo de recomendación de YouTube engancha a la gente reproduciendo vídeos con anuncios para beneficiarse económicamente, y en el camino fomenta comportamientos tan desagradables como la pedofilia. https://www.wired.co.uk/article/youtube-pedophile-videos-advertising &middot; https://www.theverge.com/2019/2/20/18233132/fortnite-ads-youtube-child-exploitive-predators-google &middot; https://www.bloomberg.com/news/articles/2019-02-20/disney-pulls-youtube-ads-amid-concerns-over-child-video-voyeurs
  - **28 de febrero de 2019**. _YouTube bans comments on all videos of children_. https://www.bbc.com/news/technology-47408969

El director general de Netflix declara que el mayor enemigo de su plataforma son las horas de sueño de sus usuarios. https://site.eightsleep.com/blogs/news/netflix-and-sleep


## Píldoras informativas

### Artículos divulgativos

**19 de junio de 2020**. _Lo que ves en tus redes sociales no está ahí por casualidad: por qué es noticia que TikTok cuente cómo ordena los vídeos_ (Maldita Tecnología). https://maldita.es/malditatecnologia/2020/06/19/redes-sociales-algoritmos-recomendaciones-tiktok-ordena-videos/

### Vídeos

**21 de septiembre de 2018**. Cómo el funcionamiento de las redes sociales explota nuestras necesidad de pertenencia a grupos sociales para engancharnos y facilita el trabajo de quienes quieren desinformar. _Why every social media site is a dumpster fire :fire:_ (Vox Media). https://www.invidio.us/watch?v=wZSRxfHMr5s (original en [Youtube](https://www.youtube.com/watch?v=wZSRxfHMr5s))

**Marzo de 2011**. Eli Pariser explica qué es la burbuja de filtros y argumenta poderosamente que resultará nocivo para nosotros y para la democracia. _Cuidado con las burbujas de filtros online_ (Eli Pariser). https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles ([transcripción](https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles/transcript))
