---
headless: true
resumen: vulnerabilidad Monsieur Cuisine
fecha: 2019-07-13
temas:
  - seguridad
sujetos:
  - silvercrest
---

El robot Monsieur Cuisine de SilverCrest vendido en Lidl oculta un micrófono en su interior y utiliza una versión obsoleta y vulnerable de Android. [eldiario.es](https://www.eldiario.es/tecnologia/Hackean-Monsieur-Cuisine-Lidl-descubren_0_909560083.html)
