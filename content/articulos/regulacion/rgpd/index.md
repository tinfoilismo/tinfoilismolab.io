---
title: Reglamento General de Protección de Datos
etiquetas:
  - eje-privacidad-seguridad
aliases:
  - /docs/regulación/rgpd
  - /wiki/rgpd/
---

El **Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016**, abreviado como **RGPD**, es el reglamento europeo relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos y deroga la Directiva 95/46/CE (Reglamento general de protección de datos). Entró en vigor el 25 de mayo de 2016 y fue de aplicación el 25 de mayo de 2018. Las multas por el no cumplimiento del RGPD pueden llegar a los 20 millones de euros.

En España, el RGPD dejó obsoleta la LOPD de 1999, siendo sustituida el 7 de diciembre de 2018 por la [LOPD-GDD]({{< relref lopd-gdd >}}), acorde con el RGPD.


El documento en el Diario Oficial de la Unión Europea está disponible para su descarga en PDF y para consula online en https://eur-lex.europa.eu/eli/reg/2016/679/oj?locale=es


---

13 things to know about the GDPR (Mozilla Blog): https://blog.mozilla.org/internetcitizen/2018/05/23/gdpr-mozilla/
