---
title: Sincroniza tu agenda con Radicale y DAVx⁵ y abandona el calendario y los contactos de Google
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/17
etiquetas:
  - eje-apertura
  - eje-descentralizacion
  - eje-privacidad-seguridad
aliases:
  - /docs/autoalojamiento/radicale
  - /wiki/radicale/
---

Sustituir en tus dispositivos los servicios del omnipresente Google es uno de los pasos más complicados de dar. Con la conveniente integración de Google Calendar y la sincronización de contactos en los dispositivos móviles y en las suites de trabajo de Google, da la sensación de que son piezas fácilmente sustituibles, pero no es así.

Vamos a explicar cómo hacerlo de la manera más sencilla para que no se convierta en un drama.

Una propuesta fácil y directa es usar [Radicale, un servidor CalDAV y CardDAV](https://radicale.org/) escrito en Python. Una solución humilde pero totalmente funcional.


## ¿Qué son CalDAV y CardDAV?

Para entender qué son CalDAV y CardDAV, primero tenemos que hablar de [WebDAV](https://es.wikipedia.org/wiki/WebDAV). De una manera simplificada, podemos describir WebDAV como un mecanismo que nos permite manipular ficheros en un servidor remoto usando como medio de comunicación el protocolo HTTP. Es decir, que subimos, movemos, renombramos y borramos ficheros sin usar FTP ni nada parecido, solamente haciendo peticiones HTTP en XML.

Bien, pues como WebDAV nos permite gestionar ficheros, a alguien se le empezaron a ocurrir diferentes aplicaciones y posibilidades. Una de ellas fue usarlo para almacenar y compartir información de calendario, a lo que llamaron CalDAV.[^caldav-estándar] [^caldav-tz-estándar] Esta extensión usa un formato estandarizado para almacenar la información del calendario que se llama [iCal](https://es.wikipedia.org/wiki/ICalendar).

Por otro lado, tenemos la extension CardDAV, que sirve para almacenar contactos, de la misma manera que CalDAV.[^carddav-estándar] CardDAV utiliza el formato estandarizado [vCard](https://es.wikipedia.org/wiki/VCard) para almacenar la información de los contactos.


## Radicale: tu servidor CalDAV y CardDAV

[Radicale](https://radicale.org/) es una implementación en Python de un servidor CalDAV y CardDAV de código libre. Es una solución austera, pero funcional. Una de las ventajas es que no necesita levantar varios servicios ni una base de datos porque utiliza el propio sistema de archivos. También dispone de una interfaz web sencilla que facilita la configuración de las cuentas. Es un servicio que se levanta y a correr.

Entonces, Radicale nos ofrece solución a dos problemas que nos surgen al abandonar los servicios de Google: "¿qué hago con el calendario?" y "¿qué hago con los contactos?".

La instalación de Radicale es bastante sencilla si estás acostumbrado a usar Python. Deberás crear un entorno virtual, instalar las dependencias con el famoso `pip` y levantar el servidor.[^radicale-config] Pero, si puedes, no te compliques la vida: usa [Docker](https://es.wikipedia.org/wiki/Docker_%28software%29) y levanta una instancia como esta:

```
docker pull mailu/radicale
docker start -d -p 5232:5232 -v /srv/radicale/config:/config -v /srv/radicale/data:/data exoslpm/radicale
```

Esta instancia te levanta un servidor Radicale en el puerto 5232, con los volúmenes bindeados en `/srv/radicale`. Como puedes ver, dentro habrá un directorio `config`, donde alojar la configuración y las cuentas de usuario, y un `data`, donde estarán almacenados todos los calendarios y contactos.

Por defecto, esta instalación Radicale espera tener un usuario y contraseña creados en el fichero `/srv/radicale/config/htpasswd` con un hash de bcrypt. Por ejemplo, para crear un usuario `tinfoil_user` con bcrypt podemos usar la herramienta htpasswd de `apache2-utils` (o bien usar algún servicio online):

```
htpasswd -B -c /srv/radicale/config/htpasswd tinfoil_user
```

![](radicale-inicio-de-sesión.png "Inicio de sesión en la interfaz web de Radicale")

Una vez que tengas el usuario creado, deberás acceder vía web a la interfaz de administración en `http://<radicale_server>:5232` con el usuario y la contraseña. Allí podrás crear tus calendarios y libretas de contactos.

![](radicale-añadir-colección.png "Formulario para crear una colección nueva en Radicale")
![](radicale-colecciones.png "Lista de colecciones (libretas y calendarios) en Radicale")


## Configurar el cliente: tu aplicación móvil

Vale, ya tenemos el servidor instalado, así que ahora nos hace falta poder usarlo. Para eso, podemos utilizar la aplicación [DAVx5 para Android](https://www.davx5.com/) de código libre, disponible de pago en Google Play o gratuítamente en [F-Droid](https://f-droid.org/en/packages/at.bitfire.davdroid/).

![](davx5-inicio.png "Pantalla principal de DAVx5")
![](davx5-acerca-de.png "Información de licencia de DAVx5")

Esta aplicación te permite sincronizar calendarios y libretas de contactos remotos (y listas de tareas) alojados en un servidor WebDAV. Conque introducciendo la dirección URL proporcionada por Radicale, junto con el usuario y la contraseña, DAVx5 sincronizará los contactos y calendarios "internos" de tu teléfono con los cambios que ocurran.[^davx5-config] De esta manera, ya no tendrás que hacer nada más, tan solo esperar a que haga su trabajo y se sincronice.

![](davx5-añadir-cuenta.png "Formulario de añadir cuenta de DAVx5")
![](davx5-configurar-cuenta.png "Crear cuenta en DAVx5")

Puedes compartir una cuenta con más gente y mantener calendarios, libretas de contactos o listas de tareas comunes, con los riesgos y ventajas que eso conlleva. La ventaja es que se pueden configurar cuentas a gusto de cada cual.


## Referencias

[^radicale-config]: Configuración básica de Radicale: https://radicale.org/setup/
[^davx5-config]: Configuración de DAVx5 con Radicale: https://www.davx5.com/tested-with/radicale
[^caldav-estándar]: RFC4791: Calendaring Extensions to WebDAV (CalDAV): https://tools.ietf.org/html/rfc4791
[^caldav-tz-estándar]: RFC7809: Calendaring Extensions to WebDAV (CalDAV) Time Zones by Reference: https://tools.ietf.org/html/rfc7809
[^carddav-estándar]: RFC6352: CardDAV: vCard Extensions to Web Distributed Authoring and Versioning (WebDAV): (https://tools.ietf.org/html/rfc6352)
