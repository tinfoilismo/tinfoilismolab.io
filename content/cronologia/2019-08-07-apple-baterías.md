---
headless: true
resumen: Apple baterías
fecha: 2019-08-07
temas:
  - reparabilidad
sujetos:
  - apple
---

**Apple** incluye una alerta en los iPhone XR, XS y XS Max con iOS 12 y 13 (beta) que alerta de que la instalación de la batería no ha sido realizada por el servicio técnico oficial, incluso aunque la batería sea un repuesto original de otro iPhone. [iFixit](https://es.ifixit.com/News/apple-is-locking-batteries-to-iphones-now)

  - **14 de agosto**. Apple argumenta que es por motivos de seguridad y sigue repitiendo que solo ocurre cuando la batería no es original, aunque ya se haya demostrado que esto último no es cierto utilizando baterías originales de otros teléfonos iPhone. [The Verge](https://www.theverge.com/2019/8/14/20805744/apple-iphone-battery-replacements-ios-safety-statement-unauthorized)
