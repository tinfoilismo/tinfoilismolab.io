---
headless: true
resumen: Multa Facebook Cambridge Analytica
fecha: 2018-10-25
temas:
  - privacidad
  - sanciones
sujetos:
  - facebook
---

Reino Unido sanciona a **Facebook** con 500.000&thinsp;£, la máxima multa prevista bajo la legalidad del momento, por fallar en proteger los datos de sus usuarios, hecho que derivó en el uso político de esos datos en el escándalo de Cambridge Analytica. [ICO (comisión de protección de datos inglesa)](https://ico.org.uk/about-the-ico/news-and-events/news-and-blogs/2018/10/facebook-issued-with-maximum-500-000-fine/)
