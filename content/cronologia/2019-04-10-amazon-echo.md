---
headless: true
resumen: Amazon Echo
fecha: 2019-04-10
temas:
  - privacidad
sujetos:
  - amazon
---

Trabajadores de **Amazon** escuchan grabaciones privadas de usuarios del asistente de voz Alexa (Amazon Echo) incluso cuando los usuarios han desactivado la compartición de grabaciones para mejorar el desarrollo, que está activada por defecto. [Bloomberg](https://www.bloomberg.com/news/articles/2019-04-10/is-anyone-listening-to-you-on-alexa-a-global-team-reviews-audio)
