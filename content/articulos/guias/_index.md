---
title: Guías
summary: Guías prácticas para mantener tu soberanía digital.
images:
  - articulos/guias/icon.svg
colors:
  dark: "#3971b1"
  light: "#b9cce2"
aliases:
  - /categoría/guías/
  - /categoría/autoalojamiento/
---

Guías prácticas para mantener tu soberanía digital.
