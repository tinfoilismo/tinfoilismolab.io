---
headless: true
resumen: Multa Facebook
fecha: 2017-05-18
temas:
  - sanciones
sujetos:
  - facebook
---

La Comisión Europea sanciona a **Facebook** con 110 millones de € por facilitar información incorrecta o engañosa sobre la adquisición de WhatsApp en 2014. [Comisión Europea](http://europa.eu/rapid/press-release_IP-17-1369_en.htm) &middot; [TechCrunch](https://techcrunch.com/2017/05/18/facebook-fined-122m-in-europe-over-misleading-whatsapp-filing/)
