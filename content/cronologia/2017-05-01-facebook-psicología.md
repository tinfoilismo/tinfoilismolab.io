---
headless: true
resumen: Facebook psicología
fecha: 2017-05-01
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** llevó a cabo un estudio con datos internos y comunicó a los anunciantes que puede identificar la vulnerabilidad emocional de los adolescentes y cuándo se sienten «inseguros» o «inútiles». Facebook responde que no ofrece herramientas para dirigir anuncios en función del estado emocional. [The Guardian](https://www.theguardian.com/technology/2017/may/01/facebook-advertising-data-insecure-teens)
