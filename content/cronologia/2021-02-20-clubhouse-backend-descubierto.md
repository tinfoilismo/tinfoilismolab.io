---
headless: true
resumen: Clubhouse backend descubierto
fecha: 2021-02-20
temas:
  - privacidad
  - seguridad
  - dark patterns
sujetos:
  - clubhouse
---

**Clubhouse**, una mezcla entre plataforma de podcasts en vivo y sala de conferencias virtual, utiliza los servicios de una startup china llamada **Agora Inc**, cuya sede está en Shanghái, para proveer toda su infraestructura de backend. Los identificadores de cada usuario y de las salas en las que participa son transmitidos en texto plano, y no ofrece tampoco ninguna garantía de que los audios se almacenen de forma segura. La aplicación pide al usuario que comparta con la plataforma toda la información referente a sus contactos (incluidos los que no usan la plataforma), lo que le permitiría obtener su grafo social y asociarlo a su comportamiento. Se utilizan dark patterns, como el de exclusividad (app solo disponible para iPhone y falsa ilusión de escasez mediante invitaciones) y de usabilidad (botón para pemitir acceso a los contactos sin la opción de rechazar). [The Guardian](https://www.theguardian.com/commentisfree/2021/feb/20/why-hot-new-social-app-clubhouse-spells-nothing-but-trouble) &middot; [Stanford Internet Observatory](https://cyber.fsi.stanford.edu/io/news/clubhouse-china) &middot; [Lourdes Turrecha en Medium](https://medium.com/privacy-technology/when-fomo-trumps-privacy-the-clubhouse-edition-82526c6cd702)
