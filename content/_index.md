---
title: Tinfoilismo
---

**Tinfoilismo** es una base de conocimiento colaborativa mantenida por gente preocupada por las derivas antihumanas de la tecnología y el mundo digital.
