---
headless: true
resumen: EMT tarjetas NFC
fecha: 2020-01-28
temas:
  - defectuoso por diseño
  - seguridad
sujetos:
  - emt
  - santander
  - visa
  - mastercard
---

La Empresa Municipal de Transportes de Madrid (**EMT**) cobra doble a los pasajeros de los autobuses que acercan a la canceladora su cartera o bolso con una tarjeta de transporte público en vigor junto con una tarjeta bancaria con tecnología sin contacto (_contactless_). El servicio de cobro sin contacto, propulsado por **Santander**, **Visa** y **MasterCard**, lleva activo 2 meses, durante los que se han recibido cuatro quejas diarias por cobros indebidos. [Cadena SER](https://cadenaser.com/emisora/2020/01/28/radio_madrid/1580234546_353235.html) &middot; [El País](https://elpais.com/ccaa/2019/02/19/madrid/1550578892_884582.html)
