---
title: Términos y condiciones y patrones engañosos de diseño
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/35
draft: true
---

Prácticamente nadie se lee los términos y condiciones de los servicios en internet. Son tediosas marañas legales e ilegibles y nos han acostumbrado a darle a un botón sin pensar. Algunos términos incluso incluyen cláusulas trol para evidenciar esto.[^1]

Los patrones opacos de diseño (_design dark patterns_)[^2] son trucos utilizados en el diseño de aplicaciones y páginas web para que el usuario realize acciones que no realizaría si estuviera bien informado o si fuera consciente de estar realizando una elección.[^3] Los patrones opacos recogen engaños desde aceptar términos y condiciones, compartir información personal como tus contactos, suscribirse a listas de correo, comprar productos a mayor precio que el inicialmente mostrado o, incluso, comprar productos extra no solicitados.


## Píldoras informativas

### Artículos divulgativos

**25 de junio de 2020**. _"Patrones oscuros": la técnica que hace que actúes contra tu voluntad en Internet_ (Maldita Tecnología). https://maldita.es/malditatecnologia/2020/06/25/patrones-oscuros-tecnica-actuar-contra-voluntad-internet/

**23 de julio de 2020**. _Puntos clave en los que fijarte en las Políticas de Privacidad (aunque lo mejor es leerlas enteras)_ (Maldita Tecnología). https://maldita.es/malditatecnologia/2020/07/23/puntos-clave-politicas-de-privacidad/


## Referencias

[^1]: 22.000 personas aceptan los términos y condiciones de un servicio de wifi gratuito que les obliga legalmente a realizar 1.000 horas de servicio comunitario y que ofrecía un premio a quien se las leyese. https://www.theguardian.com/technology/2017/jul/14/wifi-terms-and-conditions-thousands-sign-up-clean-sewage-did-not-read-small-print

[^2]: Web informativa de los distintos tipos de patrones opacos de diseño por el diseñador Harry Brignull. https://darkpatterns.org/

[^3]: Paseo por los trucos de diseño utilizados para engañarte por Tristan Harris, mago y ex especialista en ética del diseño en Google. _Cómo la tecnología secuestra tu juicio_. https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3
