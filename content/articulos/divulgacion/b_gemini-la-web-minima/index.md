---
title: Gemini, la web mínima
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/59
featured: true
etiquetas:
  - eje-privacidad-seguridad
draft: true
---

![steps.png](gemini_3.jpg "Astronautas en la misión Gemini 3. Imagen de la NASA")


El 23 de marzo de 1965[^history], los astronautas Gus Grissom y John Young se enbarcaron en la Gemini 3, que fue la primera misión tripulada del programa Gemini[^gemini], y la novena del programa espacial estadounidense. Por primera vez la tripulación usó propulsores para modificar la órbita, lo cual suponía un experimento esencial para la maniobrabilidad de los futuros viajes a la luna. La cápsula que llevaba a los astronautas fue lanzada por un cohete Titan II GLV desde Cabo Cañaveral.

El **protocolo Gemini** toma su nombre de esta misión espacial. Usa por defecto el puerto TCP/1965 en homenaje al año en el que tuvo lugar. Además, los contenidos que se sirven a través de este protocolo se conocen como “cápsulas”, haciendo referencia al artefacto en el que viajaron los astronautas.

El programa Gemini de la NASA es el sucesor del programa Mercury[^mercury], que utilizaba una cápsula minimalista donde únicamente podía viajar una persona y no permitía realizar ajustes en su órbita. Es también el antecesor del programa Apolo[^apolo], mucho más complejo, y que puso a los primeros seres humanos en la superficie de la luna. Del mismo modo que el programa Gemini, el protocolo Gemini pretende situarse entre medias de dos tecnologías preexistentes y tomar características de ambas, como veremos posteriormente.

Hasta cierto punto, Gemini pretende revivir el espíritu de la web original, que era rápida, ligera y apenas tenía usos más allá del puro intercambio desinteresado de información.

## Qué es

Gemini[^faq] [^lwn] es un protocolo muy simple, inspirado en Gopher y en HTTP. De hecho, Gemini se podría considerar al mismo tiempo un Gopher modernizado y securizado, y un HTTP básico, reducido a su esencia más pura. Sin embargo, no pretende sustituir a ninguno de los dos, sino servir de complemento para quienes buscan un camino intermedio.

Puede servir archivos de cualquier tipo, dependiendo del soporte que tenga cada navegador que lo implemente. Pero su especificación define y recomienda un tipo de archivos de hipertexto muy ligeros y que permiten ser enlazados entre sí.

Gemini intenta ser lo más simple y primitivo posible a todos los niveles, lo cual posibilita una menor carga cognitiva para quienes participan en su desarrollo y su uso. Proporciona por tanto una mayor robustez y una menor propensión a errores, además de una mayor seguridad debida a su reducida superficie de ataque.

Gemini es un protocolo cliente-servidor clásico, y se apoya en tecnologías ubicuas como las URIs, los tipos MIME y TLS.

### Quién está detrás

El proyecto Gemini comenzó en junio de 2019[^origin] de la mano de Solderpunk[^solderpunk], que hace el papel de "dictador benevolente", a imagen y semejanza de otros muchos proyectos de software libre como Linux o Python. Sin embargo, ha incorporado muchas ideas de una comunidad informal e incipiente que está en comunicación a través de Gopher y del fediverso.

### Estado actual

El protocolo Gemini se considera maduro y finalizado, y en aras de mantener su simplicidad no aceptará peticiones de nuevas características, a excepción de posibles mínimas mejoras que puedan elimninar ambigüedades y mejorar la precisión, de cara a que en un futuro pueda sea aceptado como estándar de internet por organismos como la IETF.

## Archivos de hipertexto gmi

El formato gmi, representado por el tipo MIME text/gemini, es un formato de marcado inspirado en Markdown, pero recortado, ya que no soporta imágenes en línea. Además, obliga a que cada bloque o prárrafo tenga exclusivamente un tipo de formato, incluidos los enlaces externos, que ocupan su propia linea, en lugar de estar incrustados en el texto. Esto puede resultar extraño al principio, pero facilita enormemente el parseado y simplifica y hace más robusto tanto el código como los archivos de hipertexto.

Este formato tampoco tiene soporte para estilos, con lo cual no soporta CSS ni nada similar. Se basa en la premisa de que el estilo es algo que debería estar bajo el control del lector, no de quien genera el contenido, facilitando así la accesibilidad, ya que cada cual puede elegir los colores, tamaños y tipografías que mejor se adapten a sus gustos, a las dimensiones del dispositivo desde el cual se lee, o a las posibles discapacidades visuales del usuario. Esto además permite a los creadores focalizarse en el contenido y no tener que lidiar con la presentación.

## Privacidad

Gemini minimiza el seguimiento, ya que no soporta lenguajes del lado del cliente, como es el caso de Javascript en la web. La única forma de recabar información de navegación sería observando los logs del servidor, pero en ningún caso se pueden estudiar otros comportamientos o interacciones del usuario con la cápsula.

Además, sus limitadas capacidades en este aspecto hacen que Gemini no sea del interés de empresas comerciales ni de publicidad, con lo cual tampoco existen incentivos para efectuar este rastreo.

## Seguridad

TLS

Capacidad de identificarse en sitios Gemini mediante el certificado.

## Críticas

En el mundo Gemini, el hecho de ser incompatible con HTTP se ve como algo positivo.

It feels like the main feature of this project is incompatibility with HTTP. The protocol is new and primitive, it's easy to write tools around it, write texts about it, etc. A small community forms, and you're part of it. You can advertise it to others, or rant against the mainstream, or whatever. But now what? You lose interest and abandon it, and eventually it dies.

¿Por qué no HTTP simplificado?

¿Gemini realmente sirve para algo?

Solucionismo[^solutionism]

## Especificación

La especificación habla tanto del protocolo en sí mismo como del lenguage de marcado.

Especificación[^specification]

A grandes rasgos es parecido a Gopher o HTTP/1.1

- Cada conexión se cierra al finalizar una transacción y no puede ser reutilizada.
- Se usa por defecto el puerto TCP/1965.
- Se usan [URIs](https://datatracker.ietf.org/doc/html/rfc3986/) para acceder al contenido.
- Solo hay un tipo de transacción posible
    - Es equivalente a HTTP GET
    - Siempre UTF-8
    - Su contenido ha de ser la URI seguida de <CR><LF>
    - Como máximo 1024 bytes.
    - Ejemplo: “gemini://ejemplo.com\r\n”
- Respuesta
    - Formato <STATUS><SPACE><META><CR><LF>
    - Ejemplo: “20 ”
    - Define códigos de estado de 2 dígitos
    - Meta es un MIME type definido en RFC 2046 (por defecto "text/gemini; charset=utf-8”)
- Obligatorio el uso de TLS (mínimo 1.2, recomendado 1.3)

Qué significa escuchar en un puerto? [^listen]

## Por qué usar Gemini y no HTTP/1.1 simpificado

- A favor de Gemini
    - Volver a empezar, HTTP está roto
    - Seguridad total de no estar siendo trackeados
    - Facilidad de lectura y ausencia de distracciones
    - No comercializable
- A favor de HTTP
    - Compatibilidad hacia atrás
    - En esencia es también muy simple
    - Gemini excluye a la gente que usa navegador web → elitista


## Herramientas e implementaciones

Existen una gran variedad de servidores, clientes, proxies y todo tipo de herramientas adicionales, como conversores entre diferentes lenguages de macado, extensiones para navegadores, agregadores de feeds, y muchas otras cosas. Una buena recopilación de estos recursos se puede ver aqui[^tools].

### Servidores

Hay decenas de implementaciones, algunos con soporte adicional para CGIs o virtual hosts.

#### CGIs

#### Virtual hosts

#### Servidor básico de Gemini

Como muestra de la simplicidad de Gemini, proponemos el siguiente códogo Python, que puede ser ejecutado con las versiones más recientes de Python 3 (3.7 y superiores) y no necesita dependencias de terceros (usa módulos criptográficos de la librería estándar). Lo que sí requerirá es un certificado y una clave privada asimétrica, ya sean autogeneradas por nosotros mismos o por certbot.

```python
import socket
import ssl

HOST = "127.0.0.1"
PORT = 1965

CERT_FILE = '/home/user/.ssh/gemini.crt'
KEY_FILE = '/home/user/.ssh/gemini.key'

CHUNK_SIZE = 2048

if __name__ == "__main__":

    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.load_cert_chain(CERT_FILE, KEY_FILE)

    # Create a standard TCP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((HOST, PORT))
        sock.listen(0)

        # Wrap socket inside a ssl layer
        with context.wrap_socket(sock, server_side=True) as ssock:

            # Keep the server running after finishing each connection
            while True:

                # Create a new connection
                conn, addr = ssock.accept()
                with conn:
                    print('Connected by', addr)

                    # Read from the stream until there is no data left
                    # (be careful with recv because it blocks the server)
                    received_data = ''
                    while True:
                        chunk = conn.recv(CHUNK_SIZE).decode()
                        received_data = received_data + chunk
                        if len(chunk) < CHUNK_SIZE or (len(chunk) == CHUNK_SIZE and chunk[-1] == '\n'):
                            break;

                    print(f"Received: {received_data}")

                    # Send a valid response to the Gemini client
                    conn.send("20 text/gemini\r\n".encode())
                    conn.send(f'# Congrats!\r\nYou have reached a very simple Gemini server\r\n'.encode())
```

Con este código podemos implementar un servidor que responda con un código 20 (OK) y un pequeño texto.

### Clientes

Dada la simplicidad del lenguage de marcado que usa Gemini, y la ausencia de imágenes, estilos y demás elementos gráficos, es perfectamente posible implementar navegadores exclusivamente en modo texto que funcionen en una terminal, sin perder funcionalidad. Es tan sencillo como descargar el archivo gmi de turno, y mostrar el contenido recibido, con un procesado minimo. Esto permite además que este tipo de clientes puedan ser multiplataforma sin ningún esfuerzo. En este ámbito podemos destacar algunos clientes como Anfora o Bombadillo, aunque hay multitud[^gemini_clients] de ellos, escritos en diferentes lenguajes de programación.

Podemos ver una implementación[^python_demo] de un cliente básico de Gemini en unas 100 líneas de código Python y sin dependencias externas.

Sin embargo, también existen múltiples clientes[^geminiquickstart] en modo gráfico, tanto para los tres principales sistemas operativos de PC como para móviles (Android e iOS). Destaca en este ámbito Lagrange, basado en SDL, que también es multiplataforma y ofrece una interfaz muy cuidada y agradable. Lagrange está también disponible para Android a través de un repositorio[^lagrange_fdroid] que podemos agregar a F-Droid.

### Proxies

Para facilitar la entrada al mundo Gemini, y para los usuarios que no tienen un cliente Gemini instalado en su sistema, existe una forma de acceder al espacio Gemini mediante un navegador web HTTP, y es accediendo a través de un proxy. Un proxy no es más que una pasarela que traduce tanto el protocolo como el lenguaje de marcado. Existen varios proxies[^proxy_1] abiertos en la web.

También podemos alojar nuestro propio proxy. Uno de los proxies más usados se llama Kineto[^kineto].

## Futuro

Miedo a que los navegadores implementen demasiadas características y se convierta en una nueva web

## Más

¿Hablar sobre el protocolo Titan, para hacer subidas?

Búsqueda de artículos en HackerNews https://hn.algolia.com/?q=gemini

## Referencias

[^history]: _Flight Tests. January 1965 through December 1965 (en inglés)_ https://history.nasa.gov/SP-4002/p3a.htm

[^gemini]: _NASA. Gemini 3 Mission (en inglés)_ https://www.nasa.gov/centers/marshall/history/gemini3.html

[^mercury]: _Programa Mercury_ https://es.wikipedia.org/wiki/Programa_Mercury

[^apolo]: _Programa Apolo_ https://es.wikipedia.org/wiki/Programa_Apolo

[^faq]: _Project Gemini FAQ_ https://gemini.circumlunar.space/docs/faq.gmi

[^lwn]: _LWN.net. Visiting another world (en inglés)_ https://lwn.net/Articles/845446/

[^origin]: _Solderpunk's Gemini Protocol_ https://alexschroeder.ch/wiki/2019-06-21_Solderpunk's_Gemini_Protocol

[^solderpunk]: _Solderpunk in Mastodon_ https://tilde.zone/@solderpunk

[^listen]: _What does it mean to listen on a port? (en inglés)_ https://paulbutler.org/2022/what-does-it-mean-to-listen-on-a-port/

[^specification]: _Project Gemini. Speculative specification_ https://gemini.circumlunar.space/docs/specification.gmi

[^solutionism]: _Gemini is Solutionism at its Worst_ https://xn--gckvb8fzb.com/gemini-is-solutionism-at-its-worst/

[^tools]: _Awesome Gemini. A collection of awesome things regarding the gemini protocol ecosystem_ https://git.sr.ht/~kr1sp1n/awesome-gemini

[^python_demo]: _Minimal but usable interactive Gemini client in < 100 LOC of Python 3_ https://tildegit.org/solderpunk/gemini-demo-1

[^lagrange_fdroid]: _Lagrange Pre-Release F-Droid Repository_ https://skyjake.github.io/fdroid/repo/

[^geminiquickstart]: _Gemini Quickstart!_ https://geminiquickst.art/

[^gemini_clients]: _Gemini clients_ https://gemini.circumlunar.space/clients.html

[^kineto]: _Kineto. An HTTP to Gemini proxy_ https://sr.ht/~sircmpwn/kineto/

[^proxy_1]: _Proxy for Gopher and Gemini_ https://proxy.vulpes.one/
