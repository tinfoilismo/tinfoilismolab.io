---
headless: true
resumen: Bloqueo Facebook monopolio
fecha: 2019-02-07
temas:
  - sanciones
sujetos:
  - facebook
---

Alemania frena la compartición indiscriminada de datos entre servicios de **Facebook** (WhatsApp, Instagram) por considerarse una práctica monopolística. [Reuters](https://www.reuters.com/article/us-facebook-germany/facebooks-data-gathering-hit-by-german-anti-trust-clampdown-idUSKCN1PW0SW)
