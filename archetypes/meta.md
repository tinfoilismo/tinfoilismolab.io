---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date | time.Format "2006-01-02" }}
---

Este texto aparecerá a modo de sumario en el listado, y también dentro del post.

<!--more-->

Este texto ya sólo aparecerá dentro del post.
