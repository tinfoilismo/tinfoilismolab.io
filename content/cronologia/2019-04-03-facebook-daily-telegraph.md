---
headless: true
resumen: Facebook Daily Telegraph
fecha: 2019-04-03
temas:
  - desinformación
sujetos:
  - facebook
  - daily telegraph
---

**Facebook** se asocia con el medio británico **Daily Telegraph** para publicar una serie de artículos positivos sobre la empresa que la defienden en temas controvertidos como terrorismo, incitación al odio y ciberacoso con el objetivo de minimizar los _temores tecnológicos_ y su imagen pública. [Business Insider](https://www.businessinsider.sg/facebook-daily-telegraph-positive-sponsored-news-stories-2019-4/) &middot; [The Drum](https://www.thedrum.com/news/2019/04/04/facebook-pays-daily-telegraph-sponsored-stories-branded-content-play)
