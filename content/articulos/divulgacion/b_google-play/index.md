---
title: Google Play
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/64
draft: true
---

## Anatomía de Android y de un APK

- Un [archivo APK](https://decompileapk.com/how-to-understand-the-structure-of-an-apk-file-before-decompiling/) no es más que un ZIP con diferentes elementos que conforman una app (manifest, resources, bytecode DEX, etc). Se puede descomprimir o analizar con múltiples [herramientas](https://developer.android.com/tools/apkanalyzer?hl=es-419).

- Las apps de Android suelen escribirse en Java o Kotlin, se [compilan](https://vishalnechwani.medium.com/a-look-at-the-internals-of-the-build-process-f-android-apks-b5950ea21fe5) primero a Java *bytecode*, y posteriormente se traducen a [Dalvik *bytecode*](https://source.android.com/docs/core/runtime/dalvik-bytecode?hl=es) mediante el [Dex compiler](https://developer.android.com/tools/d8?hl=es-419). Este bytecode son instrucciones de bajo nivel para ser [ejecutadas](https://www.reddit.com/r/androiddev/comments/ilu7ye/the_internals_of_android_apk_build_process_article/) en una máquina virtual específica llamada Android Runtime (ART).

- El resultado final de esta compilación es susceptible de ser [reverseado y analizado](https://github.com/user1342/Awesome-Android-Reverse-Engineering) mediante múltiples herramientas.

- Android es abierto en el sentido de "mira, pero no toques". El código base es abierto (AOSP), pero las aplicaciones del core, preinstaladas y que dan acceso a múltiples servicios, no lo son. De facto, la mayoría de las apps acaban atadas al ecosistema de Google.

## Problemas de Google Play

- Las tiendas de aplicaciones son hoy lo habitual, facilitan la búsqueda y son una puerta de entrada única a todas las aplicaciones.
    - Facilita las cosas al usuario medio pero las convierte en [jardines vallados](https://es.wikipedia.org/wiki/Jard%C3%ADn_vallado_(inform%C3%A1tica)).

- Google Play requiere una cuenta de Google, lo cual supone un gran problema de privacidad. Vinculan todas tus descargas a una identidad única.
    - Google puede agregar datos sobre preferencias de aplicación, patrones de uso e incluso comportamiento dentro de la app, si los desarrolladores usan sus servicios de análisis y tracking.
    - Google puede obtener así info de intereses, comportamiento, conexiones sociales, información de salud y financiera, etc.
    - Consiguen crear perfiles extremadamente precisos e intrusivos.
    - Como la cuenta se puede usar en múltiples dispositivos, se agrega además la info de todos los demás.

- Google Play necesita [Google Play Services](https://support.google.com/android/answer/10546414?hl=es).
	- Son una colección de APIs y servicios en segundo plano propietarios.
	- Se incluyen por defecto en casi cualquier dispositivo Android.
	- Son tremendamente invasivos y se comunican constantemente con la infra de Google.

- Muchos desarrolladores se financian incluyendo código de [Google Ads](https://medium.com/capsulat/how-to-add-google-ads-to-your-android-app-the-lazy-way-d110a1d5b94a) en sus aplicaciones, que a su vez muestra publi segmentada dentro de la app basándose en estos perfilados.

- Esto muchas veces lleva a aplicaciones prácticamente inusables por la gran cantidad de publicidad que llevan y/o por los altos precios de pagos y suscripciones, lo que se conoce como [enshittification](https://en.wikipedia.org/wiki/Enshittification) o decadencia de plataformas.

- Si no queremos renunciar a ciertas funcionalidades que ofrecen los Google Play Services (siendo las [notificaciones](https://en.wikipedia.org/wiki/Firebase_Cloud_Messaging) el más significativo), siempre podemos instalar [MicroG](https://microg.org/), que es una reimplementación de estos servicios lo menos lesiva posible.

- La [ley de mercados digitales](https://es.euronews.com/my-europe/2022/03/28/que-impacto-tendra-la-ley-de-mercados-digitales-en-las-tecnologicas-y-los-consumidores) (DMA) de 2022 introduce la libre elección de tiendas de aplicaciones de software.

- El uso de tiendas de software o almacenes de aplicaciones alternativos, por sí solo, no garantiza la protección respecto a la recopilación de datos, porque el dispositivo sigue recopilando grandes cantidades de información.

