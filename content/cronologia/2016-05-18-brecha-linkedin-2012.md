---
headless: true
resumen: Brecha LinkedIn 2012
fecha: 2016-05-18
temas:
  - seguridad
  - transparencia
sujetos:
  - linkedin
---

En la brecha de 2012 de **LinkedIn** se filtraron más de 117 millones de contraseñas y direcciones de correo de usuarios que estaban mal cifrados, no 6,5 millones, como se comunicó inicialmente. [TechCrunch](https://techcrunch.com/2016/05/18/117-million-linkedin-emails-and-passwords-from-a-2012-hack-just-got-posted-online/) &middot; [VICE Motherboard](https://www.vice.com/en_us/article/78kk4z/another-day-another-hack-117-million-linkedin-emails-and-password)
