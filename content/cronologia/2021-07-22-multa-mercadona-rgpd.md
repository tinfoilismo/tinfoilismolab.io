---
headless: true
resumen: Multa Mercadona RGPD
fecha: 2021-07-22
temas:
  - algoritmos
  - privacidad
  - sanciones
sujetos:
  - mercadona
---
**Mercadona** abona una sanción de 2,5 millones de € propuesta por la Agencia Española de Protección de Datos y decide finalizar el proyecto piloto de reconocimiento facial que fue testado durante varios meses en 48 de sus tiendas. [El País](https://cincodias.elpais.com/cincodias/2021/07/22/companias/1626970580_347133.html) · [elDiario.es](https://www.eldiario.es/tecnologia/mercadona-paga-multa-2-5-millones-proteccion-datos-sistema-reconocimiento-facial_1_8161764.html)
