---
headless: true
resumen: Brecha Facebook
fecha: 2019-03-21
temas:
  - seguridad
sujetos:
  - facebook
---

Un fallo de seguridad de Facebook dio acceso durante años a 20.000 empleados a contraseñas de entre 200 y 600 millones de cuentas de usuarios, algunos desde al menos 2012. [CNBC](https://www.cnbc.com/2019/03/21/facebook-employees-had-access-to-millions-of-user-passwords.html) &middot; [Blog del periodista de seguridad Brian Krebs](https://krebsonsecurity.com/2019/03/facebook-stored-hundreds-of-millions-of-user-passwords-in-plain-text-for-years/)
