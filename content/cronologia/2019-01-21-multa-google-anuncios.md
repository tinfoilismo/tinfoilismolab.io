---
headless: true
resumen: Multa Google anuncios
fecha: 2019-01-21
temas:
  - privacidad
  - sanciones
sujetos:
  - google
---

Francia sanciona a **Google** con 50 millones de € bajo el [RGPD]({{< relref rgpd >}}) por falta de transparencia, información inadecuada y falta de consentimiento válido respecto a la personalización de los anuncios. [CNIL](https://www.cnil.fr/en/cnils-restricted-committee-imposes-financial-penalty-50-million-euros-against-google-llc) (organismo de protección de datos francés)
