---
headless: true
resumen: Reino Unido propaganda anticifrado
fecha: 2022-01-22
temas:
  - desinformación
  - privacidad
  - libertad de expresión
---
El **gobierno del Reino Unido** lanza una campaña mediática en la que trata de convencer a los ciudadanos de que el cifrado de sus comunicaciones personales está relacionado con el abuso de menores. La campaña, cuyo coste supera el medio millón de libras, es falaz y un ataque directo a la privacidad y seguridad _online_. [EFF](https://www.eff.org/deeplinks/2022/01/uk-paid-724000-creepy-campaign-convince-people-encryption-bad-it-wont-work)