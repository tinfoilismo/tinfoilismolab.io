---
headless: true
resumen: Microsoft negligencia
fecha: 2019-04-12
temas:
  - seguridad
sujetos:
  - microsoft
---

**Microsoft** rehusa arreglar urgentemente una vulnerabilidad en Internet Explorer que permite robar documentos a cualquier usuario de Windows. En protesta, el investigador de seguridad libera la vulnerabilidad. [ZDNet](https://www.zdnet.com/article/internet-explorer-zero-day-lets-hackers-steal-files-from-windows-pcs/) &middot; [Mashable](https://mashable.com/article/internet-explorer-hacker-windows-pc-exploit/)
