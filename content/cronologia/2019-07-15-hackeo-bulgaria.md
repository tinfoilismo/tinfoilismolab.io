---
headless: true
resumen: Hackeo Bulgaria
fecha: 2019-07-15
temas:
  - privacidad
  - seguridad
---

Piratean 110 bases de datos de la Agencia Tributaria de Bulgaria y filtran a los medios la información financiera de 5 millones de ciudadanos, junto con sus números de identificación nacional y sus nombres completos, de 57 de ellas. [ZDNet](https://www.zdnet.com/article/hacker-steals-data-of-millions-of-bulgarians-emails-it-to-local-media/)
