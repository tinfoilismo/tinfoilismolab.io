Este es el cuerpo del artículo.

## Título de sección

### Título de subsección

#### Título de subsubsección

No se debe usar la sección de primer nivel, ya que la cabecera h1 está reservada para el título del artículo.

### Resaltados

**Texto en negrita** y *texto en cursiva* o también _texto en cursiva_.

> Esto es un *blockquote*, es decir, un bloque de texto resaltado.
>
> Se puede usar **markdown** y dividir en párrafos dentro de un bloque resaltado.

### Enlace

Este [enlace](https://www.tinfoilismo.org) lleva a la URL entre paréntesis.

### Imagen

![Texto alternativo](imagen.jpg "Título de la imagen, que se mostrará como pie de foto y al poner el puntero encima de la imagen")

### Cita

También se puede añadir una cita[^1] a cualquier frase o palabra.

### Tabla

| Nombre  | Edad |
| ------- | ---- |
| Bob     |   27 |
| Alice   |   43 |

### Lista ordenada

1. Primer item
2. Segundo item
3. Tercero item

### Lista no ordenada

- Un item
- Otro item
- Y otro item más

### Lista anidada

- Una lista
  - Un item
  - Otro item
  - Y otro item más
- Otra lista
  - Un elemento
  - Otro elemento

## Código

```html
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Ejemplo de bloque de código</title>
  </head>
  <body>
    <p>Contenido</p>
  </body>
</html>
```

## Referencias

[^1]: Aquí enlaza la cita y puede enlazar [aquí](https://www.tinfoilismo.org) consultado por última vez el 18 de noviembre de 2015.
