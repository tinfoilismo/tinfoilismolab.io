---
headless: true
resumen: Microsoft Skype Cortana
fecha: 2019-08-07
temas:
  - privacidad
sujetos:
  - microsoft
---

Trabajadores externos de **Microsoft** escuchan llamadas privadas por Skype a través del servicio de traducción y grabaciones privadas de su asistente de voz Cortana. Los trabajadores externos pueden trabajar en las transcripcionesde audio desde sus propios domicilios. [VICE Motherboard](https://www.vice.com/en_us/article/xweqbq/microsoft-contractors-listen-to-skype-calls)
