---
headless: true
resumen: Muñeca Cayla retirada
fecha: 2017-02-07
temas:
  - privacidad
  - seguridad
---

El gobierno alemán prohibe la venta de la muñeca infantil conectada Cayla por vulnerabilidades de seguridad y obliga a los padres a destruir sus ejemplares por considerarlos dispositivos ilegales de espionaje. [BBC](https://www.bbc.co.uk/news/world-europe-39002142)
