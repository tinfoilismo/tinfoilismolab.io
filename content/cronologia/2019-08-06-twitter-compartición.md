---
headless: true
resumen: Twitter compartición
fecha: 2019-08-06
temas:
  - privacidad
sujetos:
  - twitter
---

**Twitter** revela que, desde septiembre de 2018, compartió datos de los usuarios con sus socios publicitarios, incluso si los usuarios desactivaron la personalización de anuncios, y les mostró anuncios en función de inferencias hechas sobre los dispositivos que utilizan sin su permiso. [Twitter Help Center](https://help.twitter.com/en/ads-settings) &middot; [Privacy International](https://privacyinternational.org/news-analysis/3111/twitter-may-have-used-your-personal-data-ads-without-your-permission-time-fix)
