---
headless: true
resumen: Google MasterCard
fecha: 2018-08-30
temas:
  - privacidad
sujetos:
  - google
  - mastercard
---

**Google** y **MasterCard** llegan a un acuerdo opaco de intercambio de datos en EE.&thinsp;UU. para medir el impacto de los anucios y vincular las compras _offline_ en tiendas físicas con los anuncios _online_. [Bloomberg](https://www.bloomberg.com/news/articles/2018-08-30/google-and-mastercard-cut-a-secret-ad-deal-to-track-retail-sales)
