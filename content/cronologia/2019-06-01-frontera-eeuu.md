---
headless: true
resumen: Frontera EE. UU.
fecha: 2019-06-01
temas:
  - privacidad
  - fronteras
---

Estados Unidos obligará a los solicitantes de visado a proveer nombres de perfil y cuentas de correos y números de teléfonos utilizados en los últimos cinco años. [BBC](https://www.bbc.com/news/world-us-canada-48486672)
