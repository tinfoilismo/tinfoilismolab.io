---
title: Medidas de protección de la imagen personal
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/41
draft: true
---

# Cómo funcionan los sistemas de reconocimiento.

Dejando a un lado los detalles de los distintos software, los puntos claves de identificación residen en los ojos, nariz, boca y orejas, como puede ser por ejemplo:
- Distancia entre los ojos.
- Ancho de la nariz.
- Profundidad de los ojos.
- La curvatura de las mejillas.
- La longitud de la barbilla.

Estos algoritmos toman más de 60 de este tipo de referencias (por hacernos una idea).

Hay bastantes compañías que se dedican a desarrollar este tipo de software, y otras tantas que invierten en ello (alrededor de unos 5,6 billones de dólares).

- Machine learning KSA (Arabia Saudí)
- Saudi Pentesting Company (Arabia Saudí)
- Alcatraz (US)
- Weixuanke (China)
- PAMI (China)

* Esto es una interpretación de los datos obtenidos [aquí](https://www.crunchbase.com/hub/facial-recognition-companies).


Los sistemas de monitorización buscan controlar tanto la identidad como el movimiento de esas identidades. Aquí traigo una recopilación de objetos que he podido encontrar.


* Camisetas: proyecto de 2020, testeado sobre redes neuronales de reconocimiento YOLOv2 y Faster R-CNN, se basa en la agregación de pixeles al cuerpo de manera que explota los límites de dichas redes y dificulta la asignación de una etiqueta de categorización. Actualmente las pruebas son sobre ejemplos estáticos, se está trabajando en la indetección sobre fragmentos de video.

Más info:
- [Adversarial T-shirt! Evading Person Detectors in A Physical World](https://arxiv.org/pdf/1910.11099.pdf)


* Gafas: utilizan básicamente dos técnicas: el uso de materiales reflectantes y emisión de luz infrarroja, combinadas o no. Ejemplos de esto son la investigación del National Institution of Informatics en Japón en 2015 y la campaña de Kickstarter de Reflectacles en 2016.

Más info:
- [Invisible Mask: Practical Attacks on FaceRecognition with Infrared](https://arxiv.org/pdf/1803.04683.pdf)
- [Un modelo de gafas más usable](https://faavo.jp/sabae/project/2023)
- [Face Detection in the Near-IR Spectrum](http://www.cpl.uh.edu/publication_files/J11.pdf)
- [Deep Cross Polarimetric Thermal-to-visible Face Recognition](https://arxiv.org/pdf/1801.01486.pdf)

* Monitorización del tráfico.
