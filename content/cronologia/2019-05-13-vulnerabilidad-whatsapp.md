---
headless: true
resumen: Vulnerabilidad WhatsApp
fecha: 2019-05-13
temas:
  - seguridad
sujetos:
  - facebook
---

Facebook soluciona una vulnerabilidad en WhatsApp (Android, iOS, Windows Phone y Tizen) que permitía ejecutar código remoto mediante llamadas falsas. El grupo israelí NSO Group utilizaba esta vulnerabilidad contra activistas de derechos humanos, científicos y periodistas. [Facebook](https://www.facebook.com/security/advisories/cve-2019-3568) &middot; [EFF](https://www.eff.org/deeplinks/2019/05/what-you-need-know-about-latest-whatsapp-vulnerability)
