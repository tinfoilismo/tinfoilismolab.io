---
headless: true
resumen: Multa Google YouTube
fecha: 2019-09-04
temas:
  - privacidad
  - sanciones
sujetos:
  - google
---

La Comisión Federal de Comercio de los Estados Unidos (FTC, por sus siglas en inglés) multa a **Google** con 170 millones de $ por violar la privacidad de niños en la plataforma de vídeo **YouTube**. Se recolectaban datos en vídeos orientados a niños para mostrar publicidad personalizada más tarde. [TheVerge](https://www.theverge.com/2019/9/4/20848949/google-ftc-youtube-child-privacy-violations-fine-170-milliion-coppa-ads) &middot; [NPR](https://text.npr.org/s.php?sId=757441886)
