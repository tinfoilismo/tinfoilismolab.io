---
headless: true
resumen: Brecha Google Plus
fecha: 2018-12-10
temas:
  - seguridad
sujetos:
  - google
---

Google expuso en Google+ información de perfil (nombre, correo electrónico, ocupación y edad) de 52,5 millones de usuarios a desarrolladores, y cerrará Google+ al público cuatro meses antes de lo previsto. [Blog corporativo de Google](https://www.blog.google/technology/safety-security/expediting-changes-google-plus/) &middot; [The Verge](https://www.theverge.com/2018/12/10/18134541/google-plus-privacy-api-data-leak-developers)
