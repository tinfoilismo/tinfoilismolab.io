---
headless: true
resumen: Policía reconocimiento facial
fecha: 2019-01-31
temas:
  - privacidad
  - algoritmos
sujetos:
  - policía británica
---

La policía inglesa detiene a la gente que se cubre la cara de cámaras de reconocimiento facial y multa a un hombre tras quejarse.
[The Independent](https://www.independent.co.uk/news/uk/crime/facial-recognition-cameras-technology-london-trial-met-police-face-cover-man-fined-a8756936.html)
