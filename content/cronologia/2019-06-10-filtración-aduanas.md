---
headless: true
resumen: Filtración aduanas EE. UU.
fecha: 2019-06-10
temas:
  - privacidad
  - seguridad
---

Piratean la base de datos de un sistema de reconocimiento biométrico de una subcontrata de la Oficina de Aduanas de Estados Unidos y roban decenas de miles de fotos de viajantes y matrículas de vehículos tomadas durante mes y medio a las que no debía tener acceso la subcontrata. [Buzzfeed](https://www.buzzfeednews.com/article/daveyalba/the-us-governments-database-of-traveler-photos-has-been)
