---
headless: true
resumen: Twitter teléfonos
fecha: 2019-12-24
temas:
  - privacidad
  - seguridad
  - transparencia
sujetos:
  - twitter
---

Un investigador de seguridad descubre un error en la API de **Twitter** que permite abusar del sistema que empareja números de teléfono con cuentas de usuario. Además, se evidencia que el sistema utiliza números de teléfono que el usuario aporta originalmente para la verificación en dos pasos. Solo en Europa este sistema de descubrimiento está desactivado por defecto. [TechCrunch](https://techcrunch.com/2019/12/24/twitter-android-bug-phone-numbers/)

  - **3 de febrero de 2019.** **Twitter** desvela que posibles actores estatales en Irán, Israel y Malasia han abusado del error y se disculpa con los usuarios por las consecuencias. [Blog de Twitter](https://privacy.twitter.com/en/blog/2020/an-incident-impacting-your-account-identity) &middot; [ZDNet](https://www.zdnet.com/article/twitter-says-an-attacker-used-its-api-to-match-usernames-to-phone-numbers/) &middot; [Genbeta](https://www.genbeta.com/redes-sociales-y-comunidades/twitter-reconoce-que-gran-red-cuentas-falsas-se-dedico-a-emparejar-nombres-usuario-numeros-telefono)
