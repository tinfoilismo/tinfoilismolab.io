---
headless: true
resumen: Facebook 2FA
fecha: 2019-09-27
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** da acceso a los anunciantes a datos personales que el usuario aporta por motivos de seguridad o no aporta directamente, como números de teléfono para la verificación en dos pasos o cuentas de correo electrónico que tus contactos tienen en sus agendas. [Gizmodo](https://www.gizmodo.com.au/2018/09/facebook-is-giving-advertisers-access-to-your-shadow-contact-information/)
