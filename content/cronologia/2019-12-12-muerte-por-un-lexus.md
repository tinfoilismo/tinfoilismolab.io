---
headless: true
resumen: Muerte por un Lexus
fecha: 2019-12-12
temas:
  - algoritmos
  - seguridad
sujetos:
  - toyota
---

Un hombre de 21 años murió en Nueva York después de verse atrapado entre dos coches **Toyota** Lexus IS300s del 2002. El segundo coche fue encendido en remoto por su dueño, y arremetió una segunda vez contra la víctima tras ser empujado por testigos del suceso. Toyota asegura que el modelo no incluía encendido remoto de fábrica. [BBC](https://www.bbc.com/news/technology-50756243)
