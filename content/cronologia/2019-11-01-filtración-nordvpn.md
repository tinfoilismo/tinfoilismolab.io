---
headless: true
resumen: Filtración NordVPN
fecha: 2019-11-01
temas:
  - seguridad
sujetos:
  - nordvpn
---

Se filtran más de 2000 contraseñas de usuarios de **NordVPN**. Las contraseñas fueron probablemente cruzadas de otras filtraciones, no obtenidas atacando directamente a NordVPN, pero evidencian una posible falta de cuidado por su parte a la hora de proteger activamente la higiene de contraseñas de sus usuarios. [Ars Technica](https://arstechnica.com/information-technology/2019/11/nordvpn-users-passwords-exposed-in-mass-credential-stuffing-attacks/)
