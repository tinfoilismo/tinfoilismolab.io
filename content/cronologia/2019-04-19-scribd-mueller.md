---
headless: true
resumen: Scribd Mueller
fecha: 2019-04-19
temas:
  - algoritmos
  - propiedad intelectual
sujetos:
  - scribd
---

El sistema Book ID de **Scribd** retira, por supuesta violación de propiedad intelectual, las copias del informe Mueller, un informe oficial de dominio público del Gobierno de los EE.&thinsp;UU. sobre la interferencia rusa en las elecciones presidenciales de 2016. [Quartz](https://qz.com/1599975/scribd-taking-down-the-mueller-report-is-what-eu-article-13-looks-like/)
