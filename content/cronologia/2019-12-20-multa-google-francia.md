---
headless: true
resumen: Multa Google Francia
fecha: 2019-12-20
temas:
  - monopolio
  - sanciones
  - transparencia
sujetos:
  - google
---

La Autoridad de la Competencia de Francia multa a **Google** con 150 millones de euros por abuso de posición dominante en el sector de publicidad en búsquedas. Las reglas de su plataforma Google Ads son «opacas y difíciles de entender» y las aplica «de manera injusta y aleatoria». El regulador francés también le obliga a aclarar su redacción y presentar informes de transparencia. [El Español](https://www.elespanol.com/economia/empresas/20191220/francia-multa-millones-euros-google-posicion-dominante/453455076_0.html)
