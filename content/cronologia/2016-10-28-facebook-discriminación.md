---
headless: true
resumen: Facebook discriminación
fecha: 2016-10-28
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** permite a los anunciantes excluir usuarios racialmente. [ProPublica](https://www.propublica.org/article/facebook-lets-advertisers-exclude-users-by-race)

 - **8 de febrero de 2017**. Facebook introduce una tecnología de inteligencia artificial para luchar contra los anuncios discriminatorios. [Mashable](https://mashable.com/2017/02/08/facebook-discrimination-tools-ads/)
