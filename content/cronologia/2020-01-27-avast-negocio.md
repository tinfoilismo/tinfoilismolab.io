---
headless: true
resumen: Avast negocio
fecha: 2020-01-27
temas:
  - defectuoso por diseño
  - privacidad
  - transparencia
sujetos:
  - avast
  - condé nast
  - google
  - home depot
  - intuit
  - mckinsey
  - microsoft
  - pepsi
  - yelp
---

Una investigación concluye que el programa antivirus **Avast** recopila datos privados de los usuarios y los vende a través de su subsidiaria **Jumpshot** a muchas de las mayores empresas del mundo, como **Google**, **Yelp**, **Microsoft**, **McKinsey**, **Pepsi**, **Home Depot**, **Condé Nast**, **Intuit**. Entre los datos vendidos se incluye el llamado _All Clicks Feed_, que muestra el comportamiento del usuario, los clics y el movimiento a través de los sitios web con gran precisión. [PCMag](https://www.pcmag.com/news/the-cost-of-avasts-free-antivirus-companies-can-spy-on-your-clicks) &middot; [Vice](https://www.vice.com/en_us/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation)

  - **30 de enero.** Avast termina la recopilación de datos de Jumpshot y pone fin a las operaciones de Jumpshot con efecto inmediato. [Blog de Avast](https://blog.avast.com/a-message-from-ceo-ondrej-vlcek) &middot; [Vice](https://www.vice.com/en_us/article/wxejbb/avast-antivirus-is-shutting-down-jumpshot-data-collection-arm-effective-immediately)
