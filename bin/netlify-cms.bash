#! /bin/env bash

set -e

VERSION=2.10.192
OUTPUT_D="./static/editor"

if [[ ! -f "config.yaml" ]]
then
  echo "Run this script from the root of a Hugo project."
  exit 1
fi

if [[ ! -d "$OUTPUT_D" ]]
then
  echo "Create a $OUTPUT_D directory first."
  exit 1
fi

curl \
  "https://unpkg.com/netlify-cms@$VERSION/dist/netlify-cms.js" \
  -o "$OUTPUT_D/netlify-cms.js"
