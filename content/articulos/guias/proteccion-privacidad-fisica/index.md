---
title: Protección de la privacidad física
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/56
etiquetas:
  - eje-privacidad-seguridad
aliases:
  - /docs/guías/privacidad-física
  - /wiki/privacidad-física/
---

La privacidad es un deporte de equipo, pero ¿cómo defenderse del adversario que se salta las normas sin árbitro? Por desconocimiento, descuido, malas prácticas o con alevosía, lo cierto es que la violación de la privacidad biométrica es una realidad que va _in crescendo_, desde que visitas la casa del dueño de un asistente de voz que reconoce tu huella auditiva hasta espacios privados aparentemente públicos donde las cámaras de seguridad recolectan tu fisionomía facial. Si no es evidente o no existe la elección, no te respeta como humano.

Este artículo recoge una colección de técnicas u otras maneras de limitar o protestar por el alcance de estos sistemas de reconocimiento y recolección cuando afectan a **nuestras cualidades físicas** o datos biométricos. Porque podemos deshacernos o modificar nuestros aparatos electrónicos y cuentas digitales, pero no podemos cambiar de huella dactilar, de cara o de voz tan fácilmente.


## Reconocimiento facial

### Maquillaje y peinados

[CV Dazzle](https://cvdazzle.com/) (Computer Vision Dazzle) es un conjunto de métodos de bajo coste para romper los algoritmos de visión por ordenador. Utiliza el peinado, el maquillaje y los accesorios de moda para crear un diseño al estilo de cualquier usuario con el que trata de descompensar los contrastes del color de la piel, la simetría facial y ciertos rasgos como el puente nasal para que los distintos algoritmos de reconocimiento facial fallen en su propósito de detectar ese rostro. El proyecto lo desarrolla el artista tecnólogo berlinés Adam Harvey.

[The Dazzle Club](https://i-d.vice.com/en_uk/article/jge5jg/dazzle-club-surveillance-activists-makeup-marches-london-interview
) es un movimiento londinense que utiliza las técnicas de CV Dazzle para reclamar privacidad en la segunda ciudad más vigilada del mundo.

### Ropa

[Anonymity Scarf](http://sanneweekers.nl/big-brother-is-watching-you/) es un prototipo de bufanda llena de caras que inunda con falsos positivos los sistemas de reconocimiento facial, los confunde y hace al usuario invisible. Diseñada por la artista Sanne Weekers.

[Hyperface](https://ahprojects.com/hyperface/) es un prototipo de patrón de tela con representaciones algorítmicas idealizadas de rostros que confunde a los sistemas de reconocimiento facial inundándolos con falsos positivos y convirtiendo al usuario en invisible. Diseñado por el artista tecnólogo berlinés Adam Harvey en conjunción con Hyphen Labs.

[REALFACE Glamoflage](https://web.archive.org/web/20170902081617/http://realface.s-c-n.biz/) fue una colección de prototipos de camiseta para confundir a los sistemas de reconocimiento facial mediante patrones y collages de rostros parecidos o que imitan a celebridades. No tiene como objetivo ocultar la cara del usuario, sino añadir ruido alrededor de la identidad visual para desconcertar y confundir la visión de la máquina. Diseñados por la artista [Simone C. Niquille](https://technofle.sh/).

### Máscaras

[Surveillance Exclusion Mask](http://www.jipvanleeuwenstein.nl/index.html#masker) es un prototipo de máscara de plástico transparente con la que el usuario se vuelve irreconocible para el software de reconocimiento facial sin perder su identidad y expresión facial, por lo que puede seguir interactuando con los demás. Diseñada por el artista holandés Jip van Leeuwenstein.

[Wearable Face Projector](http://jingcailiu.com/?portfolio=wearable-face-projector) es un prototipo de proyector portátil que proyecta caras encima del rostro real del portador. Diseñado por la artista y diseñadora Jing-cai Liu.

[URME Surveillance Identity Prosthetic](http://leoselvaggio.com/urmesurveillance) es una máscara híper realista e imprimible en 3D con la cara del artista Leonardo Selvaggio que permite cambiar de cara y engañar a los sistemas de reconocimiento facial.

[Privacy Visor](http://research.nii.ac.jp/~iechizen/official/research-e.html#research2c) son un prototipo de gafas con LED que impiden el reconocimiento facial. Diseñadas por el profesor universitario Isao Echizen.


## Detección de personas

### Ropa

[Stealth Wear](https://ahprojects.com/stealth-wear/) es un prototipo de serie de ropa que ocultan la temperatura corporal para evadir los sistemas de detección de personas mediante cámaras de **visión térmica** en drones. Diseñado por el artista tecnólogo berlinés Adam Harley.

[Adversarial Patches](https://arxiv.org/pdf/1904.08653.pdf) son prototipos de impresiones gráficas que pueden añadirse a la ropa o portarse para desviar la tecnología de vigilancia automatizada haciendo que fallen los sistemas de detección de personas. Diseñados por los científicos informáticos belgas Simen Thys, Wiebe Van Ranst y Toon Goedemé.


## Fotografía con flash

### Ropa

[The Flashback](https://www.wired.com/2015/02/paparazzi-proof-clothing-thats-embedded-reflective-glass/) fue una serie de ropa antipaparazzi fabricada con tejido reflectante que devuelve la luz a la cámara haciendo inútil toda fotografía con flash. Ideada por el DJ [Chris Holmes](https://www.djchrisholmes.com/) junto a la marca de ropa Betabrand, no llegó a ser comercializada.

[The ISHU](https://theishu.com/) es una serie de ropa antipaparazzi fabricada con un tejido reflectante que devuelve la luz a la cámara haciendo inútil toda fotografía con flash. Empresa fundada por el empresario Saif Siddiqui.


## Detectores de matrículas

### Ropa

[Adversarial Fashion](https://adversarialfashion.com/) es una tienda que vende ropa con patrones de matrículas de coche americanas diseñados para activar los lectores automáticos de matrículas (ALPRs), de manera que inyecten datos basura y degraden la calidad de dichos sistemas. Diseñado por la hacker y diseñadora de moda Kate Rose y presentado en la DEF CON 27.


## Reconocimiento de voz

### Complementos

[Wearable Microphone Jamming](http://sandlab.cs.uchicago.edu/jammer/) es un prototipo de brazalete capaz de desactivar los micrófonos en el entorno del usuario, incluyendo los micrófonos ocultos. Mediante la emisión de sonidos ultrasónicos, provoca que los micrófonos comerciales filtren el sonido en el rango audible. Es efectivo con todo tipo de micrófonos, como los de teléfonos móviles y asistentes de voz. Diseñado y desarrollado por la pareja de profesores de universidad e investigadores Heather Zheng y Ben Zhao, entre otros.


## Huella dactilar

### Complementos

[Un anillo con una piedra compuesta de fibras conductivas](https://www.kaspersky.com/blog/future-of-biometrics/) forman un concepto de huella dactilar única de usar y tirar. Diseñado conjuntamente por Kaspersky y el diseñador sueco de joyas Benjamin Waye.


## Píldoras informativas

### Artículos divulgativos

**21 de enero del 2022**. Efectividad práctica de algunas de estas medidas y cómo los algoritmos de reconocimiento evolucionan y las esquivan. _Usar maquillaje o prendas de ropa para evitar el reconocimiento facial: hasta qué punto funciona y cuáles son sus límites_ (Fundación Maldita). https://maldita.es/malditatecnologia/20220121/maquillaje-prendas-reconocimiento-facial/

**12 de noviembre del 2021**. Iniciativa Ciudadana Europea para la ilegalización de la vigilancia biométrica en espacios públicos. _Reclaim Your Face_ (EDRi). https://reclaimyourface.eu/es/
