---
title: Seguridad digital y armas tecnológicas
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/38
draft: true
---

Consecuencias de que agencias de inteligencia conviertan vulnerabilidades en armas que se descontrolan, en lugar de preocuparse por la seguridad. https://www.wired.com/story/eternalblue-leaked-nsa-spy-tool-hacked-world/
