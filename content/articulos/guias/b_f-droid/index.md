---
title: F-Droid, el repositorio de aplicaciones libres para Android
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/63
etiquetas:
  - eje-apertura
  - eje-descentralizacion
draft: true
---

En el ecosistema de Android, el sistema operativo de Google, la manera sobreentendida de obtener aplicaciones y actualizarlas es a través de su _tienda_ de aplicaciones Google Play (incluso cuando las aplicaciones en sí puedan ser gratuítas). Sin embargo, eso te obliga a vincular tu dispositivo con una cuenta de Google, aportar gran cantidad de información personal y contar con acceso a internet estable. Por otra parte, Google Play no distingue entre aplicaciones libres y de código abierto y las que no lo son, ni te informa de posibles características indeseables que puedan contener. Además, sus políticas les permiten vetar aplicaciones, generalmente por ir en contra del modelo de negocio de Google.

Esta guía recoge una alternativa llamada [F-Droid](https://f-droid.org/es/about/).

## Qué es F-Droid

[F&#8209;Droid](https://en.wikipedia.org/wiki/F-Droid) es una alternativa para instalar aplicaciones en dispositivos Android. En lugar de definirse como una tienda, se trata de un catálogo o repositorio de aplicaciones. Una aplicación llamada también F&#8209;Droid facilita la navegación por el catálogo desde tu dispositivo, así como la instalación de aplicaciones y el seguimiento de sus actualizaciones.

El proyecto F&#8209;Droid se fundó en 2010. El programador británico Ciaran Gultnieks creó la primera versión del cliente a partir del código fuente de otra tienda de aplicaciones alternativa, [Aptoide](https://es.wikipedia.org/wiki/Aptoide). F‑Droid es una plataforma abierta y transparente, con gran cantidad de [documentación](https://f-droid.org/es/docs/) disponible. El proyecto se sostiene gracias a [donaciones de los usuarios](https://f-droid.org/donate/).

F&#8209;Droid no requiere que te cres una cuenta ni aportes ninguna información personal para acceder al catálogo o descargar las aplicaciones. Las aplicaciones que descargues o instales [no se vinculan a tu identidad](/articulos/capitalismo-de-vigilancia/).

Podemos distinguir varias partes del proyecto:

- **El catálogo de aplicaciones de F&#8209;Droid:** es un repositorio de aplicaciones que solo incluye las que son libres y de código abierto (FOSS). El proceso de selección e inclusión garantiza que lo son. Todas las aplicaciones, además, están etiquetadas para informarte de características controvertidas.
- **La aplicación F&#8209;Droid:** permite navegar por los distintos repositorios desde un dispositivo Android, ver los detalles de las aplicaciones, instalarlas y mantenerlas actualizadas. Incluye por defecto el catálogo de aplicaciones de F&#8209;Droid. No utiliza los Servicios de Google Play.
- **La web de F&#8209;Droid:** permite navegar por el catálogo principal de F&#8209;Droid desde cualquier navegador web y ver los detalles de las aplicaciones. También permite descargar el archivo APK de cada aplicación para cualquier propósito, como inspeccionarla o instalarla en un dispositivo que no cuente con la aplicación de F&#8209;Droid.



## El repositorio principal de F&#8209;Droid

- El proyecto F&#8209;Droid compila las apps que ofrece en su repo oficial tomando el código fuente directamente de sus desarrolladores, con el objetivo de podamos verificar que la aplicación final, la que el usuario instala en su dispositivo, no contiene nada diferente ni adicional de lo que podamos ver en su código.
	- Esto implica una construcción reproducible o determinista de aplicaciones. [Reproducible builds en F&#8209;Droid](https://f-droid.org/docs/Reproducible_Builds/) - [Técnicas para generar reproducible  builds](https://reproducible-builds.org) - [Artículo de Nico Alt](https://nico.dorfbrunnen.eu/es/posts/2019/reproducir-fdroid)
	- Requiere confiar en que el servidor de F&#8209;Droid donde se realizan las compilaciones no ha sido comprometido.
	- Ocasiona que las actualizaciones tarden en salir un poco más que si se tomaran los APKs directamente de sus desarrolladores.
	- Las comprobaciones de código son básicas y e implican procesos automatizados.
	- Así se minimiza la posible entrada de malware o código no deseado, aún así hay que ir siempre con cuidado.



- Las apps FOSS no solo son de código abierto, sino que suelen promocionar otras libertades relacionadas (aunque no siempre).

	- Que los backends a los que se conectan también sean de código abierto y/o que ofrezcan la posibilidad de autohosting.
	- Que los datos que generen sean en formatos abiertos y portables, de modo que nos los podamos llevar libremente a otra app o entorno y no perder información.


Las apps están debidamente [etiquetadas](https://monitor.f-droid.org/anti-features) con sus "anti-features" o [características controvertidas](https://f-droid.org/es/docs/Anti-Features).


- No se permiten apps que lleven funciones de seguimiento o publicidad, salvo las personalizadas (no se puede evitar por ejemplo que una app mande info a una api de un desarrollador particular de forma "artesanal").
	- La detección de la publicidad se puede hacer gracias a que, como vimos antes, los nombres de los métodos de Java se mantienen en el byte-code de Dalvik, y los anuncios se insertan de forma programática usando código procedente de ciertas librerías cuyo nombre es identificable.

- Aún así, las comprobaciones que se hacen son automatizadas y básicas. Sigue siendo necesaria una mínima confianza en los desarrolladores.

- Algunas apps son muy difíciles de construir, y por eso no están en el repo oficial.

- Problemas de las apps FOSS

	- Suelen ser más "dolorosas" de usar, menos intuitivas, más sobrecargadas, con una interfaz más pobre. Esto dificulta su adopción, porque requieren más esfuerzo mental.
	- Su escaso uso también se debe a que no se percibe la importancia de la gobernanza comunitaria, la soberanía tecnológica, o el daño que nos ocasiona el extractivismo de datos, ya que son problemas muy abstractos y complejos.
	- No suele haber marketing ni inversiones millonarias, lo que hace el desarrollo lento y pausado.

### Otros repositorios no oficiales

Cualquiera puede hacer un repositorio compatible. [Repomaker](https://gitlab.com/fdroid/repomaker)

- IzzyOnDroid. Binarios obtenidos directamente de los propios desarrolladores. Algunas apps dependen de los servicios de Google Play. Publica más rápido que F&#8209;Droid - [Listado de apps](https://apt.izzysoft.de/fdroid/) -  [Info](https://apt.izzysoft.de/fdroid/index/info) - [Política de  inclusión](https://gitlab.com/IzzyOnDroid/repo/-/wikis/Inclusion%20Policy)

- Guardian Project - [Info](https://guardianproject.info/) - [Listado de apps](https://guardianproject.info/apps/)

- Otros - [Listado de repos conocidos](https://forum.f-droid.org/t/known-repositories/721)

## El cliente F&#8209;Droid

- F&#8209;Droid (oficial)
- [Droid-ify](https://droidify.eu.org/screenshots). Mejor diseño, añade repositorios no oficiales por defecto.

## Listado de apps por categorias

### Frontal alternativo de Google Play

- [Aurora Store](https://aurorastore.org/)

	- Una interfaz alternativa para Google Play que te permite acceder sin que tengas una cuenta de Google.
	- Permite *spoofear* datos como el modelo del móvil, el idioma, la localizacion, permitiendo tb descargar apps no permitidas en una región determinada.
	- Integra los análisis de Exodus Privacy para conocer los permisos y balizas de seguimiento que usan las aplicaciones antes de instalarlas.
	- Pemite filtrar apps por diferentes criterios (sin publicidad, sin pagos, que no dependan de GSF, por permisos, etc).
	- Pero cuidado, todo esto no impide que las apps que instalemos luego funcionen de forma convencional, con sus trackers, su publi, etc.
	- Además, aunque no usemos una cuenta de Google, sigue enviando info para poder funcionar, y podría hacer *fingerprinting* a partir de las apps que tenemos instaladas, por ejemplo.

- Más [interfaces alternativas](/articulos/interfaces-alternativas/) para redes sociales y servicios privativos. 

### Bloqueo de trackers y publicidad

- [DNS66](https://f-droid.org/packages/org.jak_linux.dns66/)

    - Establece un servicio VPN al que se desvían las rutas de todos los servidores DNS. A continuación, el servicio VPN intercepta los paquetes para los servidores DNS y reenvía todas las consultas DNS que no estén en la lista negra.

### Análisis de aplicaciones

- [Exodus](https://f-droid.org/es/packages/org.eu.exodus_privacy.exodusprivacy/)
    - Ayuda a conocer qué trackers, loggers y permisos están incrustados en las aplicaciones que tenemos instaladas.
	- La aplicación descarga los reportes que obtiene de la plataforma Exodus y los muestra. Estos informes han sido previamente ejecutados por la plataforma, mediante un análisis estático.

- [TrackerControl](https://f-droid.org/es/packages/net.kollnig.missioncontrol.fdroid/)

- [Warden](https://apt.izzysoft.de/fdroid/index/apk/com.aurora.warden)

### Utilidades

- [Fossify](https://search.f-droid.org/?q=fossify&lang=es)

	- Son un fork de las [Simple Mobile Tools](https://simplemobiletools.com/) de Tibor Kaputa.
	- Su autor las [vendió a ZipoApps](https://github.com/SimpleMobileTools/General-Discussion/issues/241) en diciembre de 2023. Esta empresa las cerró y pasó a comercializarlas en Google Play.
	- Salieron de F&#8209;Droid, se hizo un fork manteniendo su esencia original con el nombre de Fossify y volvieron al repo.

### Navegadores

- [Fennec](https://f-droid.org/es/packages/org.mozilla.fennec_fdroid/). Es Firefox sin la telemetría. Permite usar los plugins de Firefox (incluyendo uBlock Origin).
- [Mull](https://f-droid.org/packages/us.spotco.fennec_dos/). Es Fennec pero con opciones de mejora de la seguridad activadas por defecto. También permite instalar extensiones.

### Chat

- [Mattermost](https://f-droid.org/es/packages/com.mattermost.rnbeta/). Similar a Slack, pero con la opción de autohostearlo.
- [Element](https://f-droid.org/es/packages/io.element.android.x/). Cliente para Matrix.
- [Conversations](https://f-droid.org/es/packages/eu.siacs.conversations/). Cliente para XMPP.
- [Molly](https://molly.im). Cliente para Signal con características extra de seguridad.

### Sincronización de contactos y calendario

- [DAVx5](https://www.davx5.com/). Cliente y sincronización de CalDAV/CardDAV. Te permite [sincronizar tus contactos y calendarios sin Google](/articulos/sincroniza-agenda-radicale-davx5/).

### Mapas

- [Organic Maps](https://f-droid.org/es/packages/app.organicmaps/). Aplicación de mapas de OpenStreetMap ligera y navegador GPS que permite descargar los mapas para usarla sin conexión.
- [StreetComplete](https://f-droid.org/es/packages/de.westnordost.streetcomplete/). Busca información incompleta de los alrededores y ofrece al usuario la posibilidad de mejorarla mediante preguntas.
- [GraphHopper Maps](https://f-droid.org/packages/com.graphhopper.maps/). Planificador de rutas.
- [OSMAnd](https://f-droid.org/packages/net.osmand.plus/). Aplicación de mapas de OpenStreetMap y navegador GPS que permite descargar los mapas para usarla sin conexión. Es extremadamente completa en funcionalidades. Aunque es una aplicación de pago, en F&#8209;Droid se distribuye gratuítamente la versión completa.

### Deporte y fitness

- [Pedometer](https://f-droid.org/es/packages/org.secuso.privacyfriendlyactivitytracker/). Usa el acelerómetro para contar los pasos, siempre activo.
- [OpenTracks](https://f-droid.org/es/packages/de.dennisguse.opentracks/). Tracker deportivo. Exporta en formato GPX o KML.
- [FitoTrack](https://f-droid.org/es/packages/de.tadris.fitness). Fitness tracker. Genera y exporta en formato GPX.

### Fediverso

- [Tusky](https://f-droid.org/es/packages/com.keylesspalace.tusky/). Cliente para Mastodon.
- [PixelDroid](https://f-droid.org/es/packages/org.pixeldroid.app/). Cliente para PixelFed.
- [FunkWhale](https://f-droid.org/es/packages/audio.funkwhale.ffa/). Cliente para FunkWhale.
- [BookWyrm](https://f-droid.org/es/packages/nl.privacydragon.bookwyrm/). Cliente para BookWyrm.
- [Eternity](https://f-droid.org/es/packages/eu.toldi.infinityforlemmy/). Cliente para Lemmy.

### Correo electrónico
- [Thunderbird](https://f-droid.org/es/packages/net.thunderbird.android/). Construido sobre el clásico K-9 Mail.
- [FairEmail](https://f-droid.org/es/packages/eu.faircode.email/). Cliente de correo electrónico.

### Base de conocicimiento

- [SiYuan](https://f-droid.org/es/packages/org.b3log.siyuan/). Similar a Notion. WYSIWYG, genera markdown.
- [Logseq](https://f-droid.org/es/packages/com.logseq.app/). Similar a Obsidian. Genera markdown y otros formatos.

### Música, audio y vídeo

- [VLC](https://f-droid.org/es/packages/org.videolan.vlc/). El reproductor universal.
- [NewPipe](https://f-droid.org/es/packages/org.schabi.newpipe/). Frontal para YouTube.
- [AntennaPod](https://f-droid.org/es/packages/de.danoeh.antennapod/). Gestor de podcasts.

### Teclados

- [Simple Keyboard](https://f-droid.org/es/packages/rkr.simplekeyboard.inputmethod/)
- [FlorisBoard](https://f-droid.org/es/packages/dev.patrickgold.florisboard/)

### Juegos

- [Shattered Pixel Dungeon](https://f-droid.org/es/packages/com.shatteredpixel.shatteredpixeldungeon/). Juego RPG de mazmorras (roguelike).
- [Apple Flinger](https://f-droid.org/es/packages/com.gitlab.ardash.appleflinger.android/). Parecido a Angry Birds.
- [Lichess](https://f-droid.org/es/packages/org.lichess.mobileapp.free/). Ajedrez.

### Otras

- [Termux](https://f-droid.org/es/packages/com.termux/). Subsistema Linux con emulación de terminal y gestión de paquetes.
- [KeePassVault](https://f-droid.org/es/packages/com.ivanovsky.passnotes/). Cliente para KeePass.
- [Jitsi](https://f-droid.org/es/packages/org.jitsi.meet/). Videollamadas.
- [Aegis Authenticator](https://f-droid.org/es/packages/com.beemdevelopment.aegis/). Gestor de tokens para 2FA.
- [Padland](https://f-droid.org/es/packages/com.mikifus.padland/). Gestor de documentos colaborativos Etherpad.
- [PassAndroid](https://f-droid.org/es/packages/org.ligi.passandroid/). Visor de Passbook.
- [Nextcloud](https://f-droid.org/es/packages/com.nextcloud.client/). Cliente para Nextcloud.
- [ownCloud](https://f-droid.org/es/packages/com.owncloud.android/). Cliente para ownCloud.
