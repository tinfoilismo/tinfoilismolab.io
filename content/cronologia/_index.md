---
title: Cronología
images:
  - cronologia/icon.svg
summary: >
  Recopilación de sucesos ilustrativos de negligencias, malos usos, fiascos de la tecnología y sanciones administrativas o legales.
aliases:
  - /cronologías
---

Recopilación de sucesos ilustrativos de negligencias, malos usos y fiascos de la tecnología y también de sanciones administrativas o legales relevantes a los temas tratados en este sitio.

No pretenden ser listas exhaustivas, sino muestras representativas de apoyo. Utilizando hechos reales, desincentivamos las teorías conspiranoicas como recurso retórico o dialéctico, que merman la credibilidad.
