---
headless: true
resumen: Google Home
fecha: 2019-07-10
temas:
  - privacidad
sujetos:
  - google
---

Trabajadores externos de **Google** escuchan grabaciones privadas de sus asistentes de voz (Google Home/Nest) y la aplicación Asistente de Google sin que los usuarios sepan que sus conversaciones se almacenan. [VRT NWS](https://www.vrt.be/vrtnws/en/2019/07/10/google-employees-are-eavesdropping-even-in-flemish-living-rooms/)

 - Google lo admite en su blog: [Google](https://www.blog.google/products/assistant/more-information-about-our-processes-safeguard-speech-data/)
 - **2 de agosto**. Una autoridad alemana de protección de datos ordena a Google parar el procesamiento de datos en Europa para evaluar si cumple con el [RGPD]({{< relref rgpd >}}). [TechCrunch](https://techcrunch.com/2019/08/02/google-ordered-to-halt-human-review-of-voice-ai-recordings-over-privacy-risks/)
