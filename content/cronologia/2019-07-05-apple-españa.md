---
headless: true
resumen: Apple España
fecha: 2019-07-05
temas:
  - privacidad
  - transparencia
---

Durante 2018, el Gobierno de España solicitó información de usuarios españoles a Apple, que la entregó en base a «peticiones legales válidas» en al menos 3218 casos, más que Francia, Alemania y Reino Unido juntos. [El Mundo](https://www.elmundo.es/tecnologia/2019/07/05/5d1e54dcfc6c8376528b4642.html)
