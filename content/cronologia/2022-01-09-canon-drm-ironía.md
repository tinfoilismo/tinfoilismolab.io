---
headless: true
resumen: Canon DRM ironía
fecha: 2022-01-09
temas:
  - defectuoso por diseño
  - monopolio
  - reparabilidad
---
El fabricante **Canon** se ve obligado a instruír a sus propios clientes sobre cómo desactivar las comprobaciones «anticopia» de sus impresoras. Al verse afectado por la crisis de los semiconductores, no puede vender repuestos con los chips requeridos por su propio sistema DRM. [Tom's Hardware](https://www.tomshardware.com/news/semiconductor-shortage-canon-dump-toner-copy-protection-chips) · [El Otro Lado](https://www.elotrolado.net/noticias/tecnologia/canon-falta-chips-cartuchos-anticopia)