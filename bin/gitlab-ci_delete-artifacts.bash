#!/bin/env bash
# Source: https://stackoverflow.com/questions/42512733/remove-artifacts-from-ci-manually

# project_id, find it here: https://gitlab.com/[organization name]/[repository name]/edit inside the "General project settings" tab
project_id="10602617" # tinfoilismo/tinfoilismo.gitlab.io

# token, find it here: https://gitlab.com/profile/personal_access_tokens
token="[do not commit]"
server="gitlab.com"

# go to https://gitlab.com/[organization name]/[repository name]/-/jobs
# then open JavaScript console and paste this:
# copy([...$('a[title="Download artifacts"],a[download]').map((x, e) => /\/([0-9]+)\//.exec(e.href)[1])].join(' '))
# press enter, and then copy the result here:
# repeat for every page you want
job_ids=(xxxxxxxxx yyyyyyyyy)

for job_id in ${job_ids[@]};
do
 URL="https://$server/api/v4/projects/$project_id/jobs/$job_id/erase"
 echo "$URL"
 curl --request POST --header "PRIVATE-TOKEN:${token}" "$URL"
 echo "\n"
done
