---
headless: true
resumen: Zoom iOS filtración Facebook
fecha: 2020-03-26
temas:
  - defectuoso por diseño
  - privacidad
  - transparencia
sujetos:
  - zoom
  - facebook
---

La aplicación de videoconferencias **Zoom** para iOS manda datos analíticos a **Facebook** sin consentimiento del usuario, sin informar previamente ni figurar tal compartición en su política de privacidad. Entre los datos compartidos figuran el modelo del dispositivo del usuario, la ciudad, huso horario y operador móvil desde el que se conecta y un identificador único creado para que los anunciantes puedan dirigir anuncios personalizados. [VICE Motherboard](https://www.vice.com/en_us/article/k7e599/zoom-ios-app-sends-data-to-facebook-even-if-you-dont-have-a-facebook-account)

  - **27 de marzo.** Zoom aclara los datos que se enviaron y actualiza la aplicación eliminando por completo el SDK de Facebook y la compartición de esos datos. [Blog de Zoom](https://blog.zoom.us/wordpress/2020/03/27/zoom-use-of-facebook-sdk-in-ios-client/) &middot; [VICE Motherboard](https://www.vice.com/en_us/article/z3b745/zoom-removes-code-that-sends-data-to-facebook)
  - **31 de marzo.** Un usuario demanda a Zoom argumentando que la empresa violó las normas de protección de datos de California por no obtener el consentimiento del usuario antes de la transmisión de datos. [VICE Motherboard](https://www.vice.com/en_us/article/pke4vb/zoom-faces-class-action-lawsuit-for-sharing-data-with-facebook)
