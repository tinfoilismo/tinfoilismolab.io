---
title: Ágora Sol Radio
date: 2021-07-05
---
¡Por una cultura libre! Desentrañamos los derechos de autor en un especial de Mentes Corrientes.

<!--more-->

Los compañeros tinfoilistas Roboe y Luis participan en el especial de cultura libre de Mentes Corrientes, un programa cultural de Ágora Sol Radio. En él, hacen un ligero recorrido por los derechos de autor y la creatividad tras la llegada de internet, explican las licencias Creative Commons, definen lo que son los sistemas DRM (Digital Rights Management, gestión de derechos digitales) y hacen un poquito de apología del software libre.

Puedes escuchar el _podcast_ y consultar las notas al programa en la web de Ágora Sol Radio a través de este enlace: [**Mentes Corrientes: Especial Cultura Libre – Ep 24 Temporada II**](https://www.agorasolradio.org/podcast/mentescorrientes/especial-cultura-libre-ep-24-temporada-ii/).

Además, el programa se reemitió en las ondas FM de Granada por [Radio Almaina](https://radioalmaina.org/2021/07/06/especial-cultura-libre-ep-24-temporada-ii/), y se mantuvo durante dos semanas en la lista de los diez episodios más escuchados en la web de Ágora Sol Radio.

¡Gracias por escuchar!