---
headless: true
resumen: Brecha Cultura colectiva At the pool
fecha: 2019-04-03
temas:
  - seguridad
---

La información de usuarios de Cultura Colectiva y At the Pool, dos aplicaciones integradas con Facebook, como nombres, situación sentimental, contraseñas en texto plano e intereses, se almacenaba en el servicio Amazon S3 expuesta al público sin ningún tipo de autenticación. [Bleeping Computer](https://www.bleepingcomputer.com/news/security/540-mllion-facebook-records-leaked-by-public-amazon-s3-buckets/)
