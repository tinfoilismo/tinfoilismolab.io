---
headless: true
resumen: Facebook parlamento británico
fecha: 2018-12-05
temas:
  - privacidad
  - transparencia
sujetos:
  - facebook
---

El Parlamento Británico publica documentos internos de **Facebook** que considera de interés público porque evidencian, entre otras cosas, cómo la empresa, a su sola discreción, concede a desarrolladores acceso a los datos de los amigos de sus usuarios y también cómo Facebook escondió a los usuarios de su aplicación para Android que una actualización permitía recopilar los listados de llamadas y mensajes. [Business Insider](https://www.businessinsider.com/facebook-documents-six4three-case-published-british-parliament-2018-12)

 - Documentos originalmente liberados por el Parlamento Británico. [Parlamento Británico](https://www.parliament.uk/documents/commons-committees/culture-media-and-sport/Note-by-Chair-and-selected-documents-ordered-from-Six4Three.pdf)
