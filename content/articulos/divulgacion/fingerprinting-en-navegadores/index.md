---
title: Fingerprinting en navegadores
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/52
etiquetas:
  - eje-privacidad-seguridad
---

Una [huella dactilar](https://es.wikipedia.org/wiki/Huella_dactilar) es una característica que identifica de forma únivoca a una persona. No cambia con el tiempo, no puede modificarse y es única en el mundo, lo cual supone un método de identificación de personas muy potente. En inglés, huella dactilar se traduce como *fingerprint*. La [historia de la huella dactilar](https://notjustprice.com/blogs/blog/history-of-fingerprinting) es muy antigua, aunque ha sido en los dos últimos siglos cuando más se ha investigado y generalizado su uso.

![Huella dactilar](huella-dactilar.jpg "[«fingerprint»](https://www.flickr.com/photos/51453815@N00/2327808306) por [blvesboy](https://www.flickr.com/photos/51453815@N00) bajo licencia [CC BY-ND 2.0](https://creativecommons.org/licenses/by-nd/2.0/?ref=ccsearch&atype=rich)")

El **fingerprinting en navegadores** toma su nombre de esta técnica, ya que tiene muchas analogías. Consiste en tomar ciertas caracerísticas de nuestro navegador, de forma que lo hagan único e identificable y, por tanto, rastreable.

Pero hay diferencias. El uso de la huella dactilar requiere ciertas garantías judiciales y se entiende su funcionamiento por parte de la población. El fingerprinting en navegadores es una técnica bastante oscura por varios motivos, principalmente por la complejidad que tiene, ya que se puede lograr mezclando muchas características diferentes, no todas ellas fáciles de entender para gente con formación técnica insuficiente.

En definitiva, y en la práctica, la ofuscación juega a favor de quienes se benefician del lucrativo negocio de la publicidad y el rastreo sin tener que rendir cuentas. En este artículo se pretende arrojar luz sobre los mecanismos existentes y las posibles formas de mitigación.

## Canvas

El fingerprinting a través del canvas funciona explotando el elemento [`<canvas>`](https://developer.mozilla.org/es/docs/Web/API/Canvas_API/Tutorial/Basic_usage) de HTML5. Cuando un usuario visita un sitio, al navegador se le pide que dibuje un gráfico en un canvas oculto. Este gráfico se codifica entonces en [base64](https://varionet.wordpress.com/2009/11/06/sobre-base64-para-que-usarlo-para-que-no-y-como-manejarlo-en-net-y-asp-net/) mediante el método [`toDataURL()`](https://developer.mozilla.org/es/docs/Web/API/HTMLCanvasElement/toDataURL) del canvas. A su vez se calcula el [hash](https://latam.kaspersky.com/blog/que-es-un-hash-y-como-funciona/2806/) de esta codificación, dando lugar a una cadena de caracteres que potencialmente será única. Todo esto sin necesidad de tener nada almacenado en la máquina, ya que se genera cada visita, pero será siempre el mismo. Las diferencias de rendimiento asociadas a nuestra GPU, el driver que usa, nuestro sistema operativo, o nuestro dispositivo en general son lo que causa variaciones en el pintado del canvas, y por tanto, en el token resultante respecto a otros usuarios. También se puede obtener información sobre el hardware gráfico, tanto el modelo como características técnicas.

Este sistema se detalló por primera vez en un paper[^paper-canvas-fingerprinting-1][^paper-canvas-fingerprinting-2] y ha sido explotado por numerosos sitios web, con el agravante de que el usuario no es consciente en ningún momento de que esto se está produciendo, al estar el canvas oculto.

## Tipografías

Las hojas de estilos (CSS) establecen qué tipografía usar para cada elemento renderizado por el navegador. Estas tipografías pueden obtenerse del propio sistema o cargarse desde nuestro propio servidor, o un servidor de terceros. En cualquier caso, el navegador tiene formas de [averiguar qué fuentes hay instaladas](https://stackoverflow.com/questions/3368837/list-every-font-a-users-browser-can-display) en el sistema mediante métodos indirectos, lo cual provoca que, cuantas más fuentes instalemos, más única será nuestra huella.

## Historial de navegación

No es posible acceder al historial de navegación de un usuario mediante Javascript, ya que [la API no lo permite](https://developer.mozilla.org/es/docs/Web/API/History_API). Lo único que se puede hacer es sobreescribir el historial de navegación, o bien hacer que el navegador vaya a puntos concretos del historial, pero sin saber qué sitio web se visitó ahí. Hacer esto implica abandonar el sitio web actual.

En definitiva, el historial de navegación no es algo que se pueda utilizar para hacer fingerprinting.

## Estrategias de mitigación

No hay una bala de plata para eliminar el fingerprinting. No hay una única configuración que podamos establecer, ni un único plugin que podamos instalar, que nos puedan garantizar nada al 100%. Pero podemos utilizar una serie de estrategias para dificultar el proceso.

Por una parte, podemos intentar reducir nuestra "singularidad", es decir, exponer la mínima cantidad de características por las que nos puedan trazar, y dentro de estas, que sus valores sean lo más convencionales posibles. Así nos pareceremos lo máximo posible al común de los usuarios.

Por otra parte, podemos añadir ruido y aleatoriedad a las mediciones. Es decir, se trataría de falsearlas para conseguir que, en cada ejecución, estas características fuesen diferentes. Si un único usuario genera múltiples huellas se vuelve bastante más difícil de trazar, ya que en cada ejecución sería como empezar de nuevo a recopilar datos.

A continuación se detallan algunas estrategias más concretas:

- **Usar una versión popular de un navegador web popular**. Elegir Chrome o Firefox en sus últimas versiones estables (ya que suelen actualizarse automáticamente) es probablemente la mejor opción aquí.
- **Instalar algunos plugins o extensiones** puede ayudar contra el fingerprinting. Pueden anonimizar o trucar ciertas características de nuestro navegador. Por ejemplo, [uBlock Origin](https://ublockorigin.com/) o [Privacy Badger](https://privacybadger.org/es/). Pero esto puede ser también un arma de doble filo si se abusa de ello, ya que tener demasiados plugins nos hace también más únicos. Por suerte, la antigua API que daba cuenta de la lista de plugins instalados [ya no funciona](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorPlugins/plugins).
- **Eliminar Flash y similares.** Flash ha sido siempre muy vulnerable a este tipo de ataques. Por suerte está en fase de extinción y los principales navegadores [ya](https://blogs.windows.com/msedgedev/2020/09/04/update-adobe-flash-end-support/) [no](https://support.google.com/chrome/answer/6258784?hl=en) [lo](https://support.mozilla.org/en-US/kb/end-support-adobe-flash) soportan. Lo mismo podemos decir de Silverlight, una tecnología de Microsoft que nació para competir con Flash y que también ha alcanzado [su final](https://support.microsoft.com/en-us/windows/silverlight-end-of-support-0a3be3c7-bead-e203-2dfd-74f0a64f1788).
- **Considerar deshabilitar Javascript.** Esta sería una de las herramientas más eficaces, ya que mediante las APIs de Javascript se pueden obtener muchas características. Sin embargo, es algo muy difícil de asumir, ya que en la web moderna son muy habituales las webs que únicamente funcionan con JS activado, o que ofrecen una funcionalidad muy limitada si se desactiva. Sin embargo, sí que podemos tener algún plugin, como por ejemplo [Disable Javascript](https://addons.mozilla.org/en-US/firefox/addon/disable-javascript/), que nos permite activarlo y desactivarlo en páginas concretas. Así, si sospechamos de alguna web en particular, y su funcionalidad sin JS nos resulta suficiente, podemos mantener el JS desactivado permanentemente para su dominio. Deshabilitar Javascript a nivel global también es posible. Se puede hacer desde las opciones de configuración del navegador y sin necesidad de plugins. Pero es una opción poco realista por los motivos comentados anteriormente.
- **Instalar la extensión CanvasBlocker**. Se puede obtener [aquí](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker). Ahora bien, esto neutralizará también el uso de Canvas para usos legítimos.
- **Compartimentalizar nuestra navegación.** Podemos tener dos o más navegadores diferentes instalados, o incluso de diferentes versiones, y usarlos para objetivos diferentes.
- **Usar Tor Browser** [enlace al artículo de TOR]. Este navegador, además de conectarse a la red TOR para enmascarar el origen de la petición, está configurado para tener la mayor cantidad de características comunes con otros usarios (por ejemplo, un tamaño de ventana fijo que se aconseja no alterar, idioma inglés, y otras similares [ref]).
- **El uso de una VPN** ocultará nuestra IP, pero la IP de origen es sólo una característica más de las muchas que forman un fingerprint. Con lo cual ayuda, pero no demasiado.
- **El modo incógnito** del navegador solo afecta algunas de las características. Está diseñado para que un usuario [no deje rastro de su navegación en el dispositivo](https://www.mozilla.org/en-US/firefox/browsers/incognito-browser/) desde el cual se utiliza, de modo que es útil para proteger la privacidad si hay varios usuarios que lo comparten. Pero no enmascara el origen de la petición, ni altera o deshabilita las características que se pueden obtener mediante las APIs del navegador.
- **Deshabilitar la geolocalización**.
- **Deshabilitar las cookies, o al menos las cookies de terceros**.

## Herramientas

- [Browserleaks](https://browserleaks.com/) es una herramienta que pone a prueba nuestro navegador.

## Otros

https://developer.mozilla.org/en-US/docs/Web/Security/Referer_header:_privacy_and_security_concerns
https://privacysavvy.com/security/safe-browsing/browser-fingerprinting/
https://www.youtube.com/watch?v=OTt1AVRQyx0


https://www.husseinnasser.com/2019/06/transport-layer-secuirty-http-https-tls.html

## La RGPD y el fingerprinting

https://www.eff.org/deeplinks/2018/06/gdpr-and-browser-fingerprinting-how-it-changes-game-sneakiest-web-trackers

## Enlaces

https://medium.com/slido-dev-blog/we-collected-500-000-browser-fingerprints-here-is-what-we-found-82c319464dc9

Entropia de Shannon
https://en.wikipedia.org/wiki/Entropy_(information_theory)

## Referencias

[^paper-supercookies]: Konstantinos Solomos, John Kristoff, Chris Kanich y Jason Polakis (University of Illinois at Chicago. Feb 2021) [Tales of Favicons and Caches: Persistent Tracking in Modern Browsers](https://www.cs.uic.edu/~polakis/papers/solomos-ndss21.pdf).

[^paper-canvas-fingerprinting-1]: Keaton Mowery y Hovav Shacham (IEEE Computer Society, May 2012) [Pixel Perfect: Fingerprinting Canvas in HTML5](https://hovav.net/ucsd/papers/ms12.html)

[^paper-canvas-fingerprinting-2]: Gunes Acar, Christian Eubank, Steven Englehardt, Marc Juarez, Arvind Narayanan y Claudia Diaz (Nov 2014) [The Web never forgets: Persistent tracking mechanisms in the wild](https://securehomes.esat.kuleuven.be/~gacar/persistent/index.html).

http://yinzhicao.org/TrackingFree/crossbrowsertracking_NDSS17.pdf
