---
headless: true
resumen: INE rastreo teléfonos
fecha: 2019-10-29
temas:
  - privacidad
sujetos:
  - ine
  - movistar
  - orange
  - vodafone
---

El **Instituto Nacional de Estadística** de España (INE) quiere rastrear durante ocho días los teléfonos móviles de los ciudadanos con fines estadísticos. **Vodafone** (y Lowi), **Movistar** y **Orange** (y Amena) le proporcionarán datos agregados anonimizados de sus clientes.
[Comunicado de prensa del INE](https://www.ine.es/prensa/comunicado29102019.pdf) &middot; [Público.es](https://www.publico.es/sociedad/privacidad-ine-quiere-rastrear-posicion-moviles-pese-impide-ley.html) &middot; [Maldita.es](https://maldita.es/maldita-te-explica/el-ine-quiere-rastrear-todos-los-moviles-durante-ocho-dias-para-desarrollar-sus-estudios-de-movilidad-cuales-son-los-riesgos-y-como-evitar-que-usen-tus-datos/) &middot; [El País](https://elpais.com/economia/2019/10/28/actualidad/1572295148_688318.html)

  - Los días seleccionados son: en 2019, del 18 a 21 de noviembre, 24 de noviembre y 25 de diciembre (festivo nacional); y en 2020, 20 de julio y 15 de agosto.
  - **30 de octubre.** La Agencia Estatal de Protección de Datos (AEPD) solicita información sobre los protocolos establecidos con las operadoras. [El Independiente](https://www.elindependiente.com/economia/2019/10/29/la-agencia-de-proteccion-de-datos-no-se-fia-y-pide-explicaciones-al-ine-sobre-el-rastreo-de-moviles/)
