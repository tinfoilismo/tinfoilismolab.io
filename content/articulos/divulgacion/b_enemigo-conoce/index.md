---
title: La verdad sufre SDRA
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/45
draft: true
---

## La estrategia de un mar de piezas y un sólo puzzle

Hay un colectivo mejor pagado que el nuestro que trabaja con un conocimentos sesgado en un fin común. Se estudian aspectos muy dispares, algunos aparentemente irrelevantes, que condicionan significamente el comportamiento de "la masa". Este ente que respalda ideas, sanciona opiniones, un volumen relevante facilmente reconducible desde una posición más alta, estratégicamente hablando. Es vulnerable a estos condicionamientos externos.

En esto consiste este puzzle, buscar entre los millones de piezas y hallar el contexto por la que fue cortada de esa forma y no de otra. Esto es lo que Peirano creo que buscaba, advertir la existencia de este grafo de eventos y estudios con un fin.

Hay premisas que trata en el libro, tales como:

### Un mensaje adaptado al perfil receptor, recopilado y estudiado previamente. Dicho mensaje cada vez más secreto.


### Campaña de desinformación, promoción automatizada de escándalos, normalmente con pocas fuentes o dudosas.
### El mundo es un pañuelo, los que ayudaron a Trump ayudan a otros.
### Desde el aroma a las notificaciones push.

Hay una serie de empresas que tienen acaparado el mercado del aroma: Givaudan, Firmenich, Symrise e International Flavors and Fragances (IFF). Esta industria tiene dos vertientes, la más llamativa que es la alta perfumería (con el ambergris y sofisticaciones) y por otro la de los aromatizantes y saborizantes.
No sólo pensamos en el clásico ejemplo de la gominola hechas de nudillos de cerdo cocidos que se vuelve apetecible. Otras simulaciones alimentarias es la de las cápsulas de café Nespresso que recuerda a la preparación de un café en una baristerá. Otras empresas alejadas de la industria de la alimentación lo usan, como British Airways usando una fragancia que recuerda al olor del césped recién cortado y mar en sus aviones, o Singapore Airlines en sus toallistas calientes. También en los coches usan olores propios de tapicerías de cuero y acabados en madera para engañar a nuestro cerebro sobre la gama del vehículo. Los museos utilizando un aroma llamado Comme des Garçons 2. Todo esto se califica como "scent marketing".

### Las bondades de lo gratis se paga (Internet en el tercer mundo, aplicaciones).
### Los perfilados a lo Black Mirror. Los nicks son lujos. Eres tú, siempre.
### La pérdida de rigor. Lo coloquial ahora es noticia. El sensacionalismo como forma de generar empatía. Se pierde el mensaje.
### Como la primavera árabe empezó en las redes.
### Grupos radicales implsados por bots para generar disputas.
### El poder como interposición entre las relaciones frágiles.
### Condicionamiento Skinner, la teoria de los mónos, la idea inicial era para ayudar a dejar de fumar. Responsable, bondadoso e ingenuo.
### ARPANET, gestión del tráfico.
### Amazon datacenters, cable.
### Mejor pedir perdón que pedir permiso.
### Jobs, Oreilly, Opensource y software libre. GPL. Stallman.
