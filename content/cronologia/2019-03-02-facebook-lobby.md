---
headless: true
resumen: Facebook lobby
fecha: 2019-03-02
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** presionó a políticos de todo el mundo contra las legislaciones de privacidad, como la [RGPD]({{< relref rgpd >}}), prometiendo incentivos y amenazando con retirar inversiones. [The Guardian](https://www.theguardian.com/technology/2019/mar/02/facebook-global-lobbying-campaign-against-data-privacy-laws-investment)
