---
headless: true
resumen: YouTube Syrian Archive
fecha: 2019-04-24
temas:
  - algoritmos
sujetos:
  - google
---

Entre 2012 y 2018, los filtros automáticos de **YouTube** retiraron un 10% de los vídeos de Syrian Archive, un proyecto dedicado a documentar las violaciones de derechos humanos en Siria, calificándolos de «violento extremismo», confundiendo así el contexto de documentación del proyecto con el de desinformación y reclutamiento de grupos terroristas. [EDRi](https://edri.org/what-the-youtube-and-facebook-statistics-arent-telling-us/)
