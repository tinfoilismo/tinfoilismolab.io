---
headless: true
resumen: TJUE Facebook widgets
fecha: 2019-07-29
temas:
  - privacidad
  - sanciones
sujetos:
  - facebook
---

El Tribunal de Justicia de la Unión Europea dictamina que los dueños de los sitios web que incluyan botones «me gusta» de **Facebook** serán responsables de la recopilación de datos personales, según el [RGPD]({{< relref rgpd >}}). [Comunicado de prensa del TJUE](https://curia.europa.eu/jcms/jcms/p1_2278332/en/) &middot; [The Register](https://www.theregister.co.uk/2019/07/29/eu_gdpr_facebook_like_button) &middot; [Euractiv](https://www.euractiv.com/section/digital/news/sites-using-facebook-like-button-liable-for-data-eu-court-rules/)
