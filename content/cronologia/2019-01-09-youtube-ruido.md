---
headless: true
resumen: YouTube ruido
fecha: 2019-01-09
temas:
  - algoritmos
sujetos:
  - google
---

El sistema Content ID de **YouTube** marca una grabación de prueba de un micrófono como contenido protegido por propiedad intelectual.
[EFF](https://www.eff.org/takedowns/mistake-so-bad-even-youtube-says-its-copyright-bot-really-blew-it)
