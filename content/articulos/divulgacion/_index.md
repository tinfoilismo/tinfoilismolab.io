---
title: Divulgación
summary: Descubre conceptos sobre el uso de la tecnología y los datos.
images:
  - articulos/divulgacion/icon.svg
colors:
  dark: "#a83e3e"
  light: "#ddbebe"
aliases:
  - /categoría/alfabetización/
---

Descubre conceptos sobre el uso de la tecnología y los datos.
