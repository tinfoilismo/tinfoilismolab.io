---
headless: true
resumen: Facebook multa reconocimiento facial
fecha: 2020-01-30
temas:
  - privacidad
  - sanciones
sujetos:
  - facebook
---

**Facebook** paga 550 millones de dólares americanos en un acuerdo extrajudicial por vulnerar las leyes de privacidad biométrica en Illinois, Estados Unidos, con su herramienta de sugerencia de etiquetado de fotos por reconocimiento facial sin pedir permiso a sus usuarios. [ElOtroLado](https://www.elotrolado.net/noticias/internet/facebook-pagara-millones-violacion-privacidad) &middot; [The New York Times](https://www.nytimes.com/2020/01/29/technology/facebook-privacy-lawsuit-earnings.html)
