---
headless: true
resumen: Zoom cifrado engañoso
fecha: 2020-03-31 
temas:
  - desinformación
  - privacidad
  - seguridad
  - transparencia
sujetos:
  - zoom
---

Se clarifica que la aplicación de videoconferencias **Zoom**, a pesar de anunciarse en su web asegurando que las comunicaciones de la aplicación se realizan cifradas de extremo a extremo, en realidad se realizan con un cifrado de transporte (TLS). Este tipo de cifrado de transporte, más sencillo, es regularmente utilizado en internet (se conoce como HTTPS para páginas web), y sí permite que el proveedor acceda a las comunicaciones. [The Intercept](https://theintercept.com/2020/03/31/zoom-meeting-encryption/)

  - **1 de abril.** Zoom clarifica el cifrado utilizado en su servicio de videoconferencias. [Blog de Zoom](https://blog.zoom.us/wordpress/2020/04/01/facts-around-zoom-encryption-for-meetings-webinars/)
