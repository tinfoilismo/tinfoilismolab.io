---
headless: true
resumen: Facebook experimento
fecha: 2014-06-29
temas:
  - privacidad
sujetos:
  - facebook
# enlace roto a "burbuja de filtros"
---

**Facebook** llevó a cabo un experimento psicológico secreto e intrusivo a 689.000 usuarios en el que mediante el ajuste del [algoritmo que decide qué publicaciones mostrarte en la cronología](burbuja-de-filtros) influyeron en el estado de humor de sus usuarios por contagio emocional. [The Guardian](https://www.theguardian.com/technology/2014/jun/29/facebook-users-emotions-news-feeds) &middot; [The New York Times](https://www.nytimes.com/2014/06/30/technology/facebook-tinkers-with-users-emotions-in-news-feed-experiment-stirring-outcry.html)
