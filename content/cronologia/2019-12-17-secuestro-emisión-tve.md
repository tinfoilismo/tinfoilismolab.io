---
headless: true
resumen: Secuestro emisión TVE
fecha: 2019-12-17
temas:
  - desinformación
  - seguridad
sujetos:
  - tve
  - russia today
---

El jueves 12 de diciembre, **Televisión Española** (**TVE**) sufrió un ataque informático que resultó en el secuestro de la emisión del canal +24 y emitieron una entrevista del expresidente ecuatoriano Rafael Correa a Carles Puigdemont, realizada por **Russia Today**, un canal internacional con sede en Moscú acusado múltiples veces de prácticas de desinformación. [La Vanguardia](https://www.lavanguardia.com/television/20191217/472289983271/hackeo-tve-puigdemont-entrevista-correa-rt-russia-today.html)
