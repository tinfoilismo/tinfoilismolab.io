---
headless: true
resumen: Filtración Facebook
fecha: 2019-12-19
temas:
  - privacidad
  - seguridad
sujetos:
  - facebook
---

Encuentran una base de datos sin protección con información confidencial de 267 millones de usuarios de **Facebook**. La mayoría de usuarios afectados son estadounidenses y entre los datos filtrados se incluye su ID de usuario de Facebook, su teléfono móvil y su nombre completo. Se sospecha que la base de datos se compiló abusando de la API para desarrolladores de Facebook. [Comparitech](https://www.comparitech.com/blog/information-security/267-million-phone-numbers-exposed-online/)
