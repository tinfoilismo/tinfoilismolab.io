---
title: Efecto red y jardines vallados
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/34
draft: true
---

<figure>

> Last thing I remember,
> I was running for the door.
> I had to find the passage back to the place I was before.
> "Relax," said the night man,
> "We are programed to receive.
> You can check out any time you like
> But you can never leave."

<figcaption><cite>Hotel California</cite>, los Eagles.</figcaption>
</figure>


Entrar en las plataformas de redes sociales es muy sencillo, pero tomar la decisión de salir y renunciar a ellas es tremendamente difícil;[^1] están tan arraigadas en el tejido social, que incluso algunas personas no pueden permitírselo y quedan atrapadas.[^2]


## Referencias

[^1]: _Navigating the ‘Hotel California’ effect of social platforms_. https://digiday.com/marketing/navigating-hotel-california-effect-social-platforms/

[^2]: _El privilegio de cerrar Facebook_ (ONG Derechos Digitales) https://www.derechosdigitales.org/12820/el-privilegio-de-cerrar-facebook/

