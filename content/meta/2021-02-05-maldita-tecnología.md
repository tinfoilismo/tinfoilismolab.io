---
title: Maldita Tecnología
date: 2021-02-05
---
¡Maldita Tecnología! Aportamos luz con Maldita.es sobre los motores de búsqueda en internet.

<!--more-->

La Fundación Maldita.es contra la desinformación nos ha pedido prestados nuestros «superpoderes», como ellos denominan al conocimiento, para redactar un artículo divulgativo que explique cómo funcionan los motores de búsqueda en internet. El más conocido de estos motores de búsqueda es Google Search, el buscador de [la gran multinacional](https://tinfoilismo.org/sujetos/google/), aunque también tienen su parte del pastel los de [Yahoo](https://tinfoilismo.org/sujetos/yahoo/) y [Microsoft](https://tinfoilismo.org/sujetos/microsoft/) (Bing).

Puedes leer el artículo, escrito con la colaboración de nuestro compañero tinfoilista Jorge y cuyo contenido está licenciado con Creative Commons BY-SA, en el siguiente enlace: [**Motores de búsqueda en internet: cómo funcionan y por qué algunos parecen arrojar resultados menos relevantes**](https://maldita.es/malditatecnologia/20210205/funcionan-motores-busqueda-algunos-parecen-arrojar-resultados-menos-relevantes/).

La pregunta no es nada banal, pues los algoritmos o reglas que hacen funcionar estos motores son **opacos** por diseño.

¡Maldita opacidad!