---
headless: true
resumen: Brecha Facebook
fecha: 2019-09-28
temas:
  - seguridad
sujetos:
  - facebook
---

Facebook descubre una brecha de seguridad que puede haber revelado información de 50 millones de cuentas por un error en la característica «ver como». [Blog corporativo de Facebook](https://newsroom.fb.com/news/2018/09/security-update/) &middot; [El Periódico](https://www.elperiodico.com/es/sociedad/20180928/facebook-seguridad-7060414) &middot; [The Verge](https://www.theverge.com/2018/9/28/17914524/facebook-bug-50-million-affected-security-token-access-view-as-feature)

  - **12 de octubre**. Facebook reduce la cifra de afectados a 30 millones de cuentas tras su investigación interna y pone a disposición una herramienta para comprobar si has sido afectado. [Blog corporativo de Facebook](https://newsroom.fb.com/news/2018/10/update-on-security-issue/) &middot; [Xataka](https://www.xataka.com/seguridad/informacion-privada-30-millones-usuarios-riesgo-reciente-hackeo-a-facebook-fbi-investigara-caso)
