---
headless: true
resumen: Multa Booking Hungría
fecha: 2020-04-28
temas:
  - dark patterns
  - sanciones
sujetos:
  - booking.com
---

La Autoridad de la Competencia Húngara (GVH) multa a **Booking.com** con 2&thinsp;500 millones de forintos (alrededor de 6,1 millones de £) por prácticas comerciales injustas, incluyendo anuncios engañosos y presión psicológica sobre los consumidores, como promocionar cancelaciones gratuitas cuando no estaban permitidas para todas las reservas y se incluía una prima extra en los precios por ese concepto. [Reuters](https://www.reuters.com/article/us-booking-hldg-hungary-idUSKCN22A3AZ)
