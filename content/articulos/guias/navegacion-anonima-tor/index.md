---
title: Navegación anónima con Tor
featured: true
etiquetas:
  - eje-inclusion
  - eje-privacidad-seguridad
aliases:
  - /docs/guías/tor
  - /wiki/tor/
---

[Tor](https://www.torproject.org/es/) es el nombre que recibe, por sus siglas, _The Onion Router_ («el router cebolla»), un sistema de comunicación sobre internet que permite un alto nivel de anonimidad. A través de un sistema de capas de cifrado, los usuarios no revelan datos identificativos sobre quiénes son. También permite eludir la censura que algunos gobiernos ejercen sobre internet. Todo el software del Tor Project es software libre.

Puede consultarse [aquí](https://media.torproject.org/video/2015-03-animation/HD/Tor_Animation_HD_es.mp4) un sencillo vídeo explicativo.[^tor-animation]

## Tor Browser

El uso más sencillo que los usuarios pueden hacer de Tor es navegar por la web anónimamente a través del Tor Browser, un navegador web ya preparado para utilizar la red Tor. El Tor Browser también inutiliza la huella digital que los usuarios puedan dejar en los sitios que visitan porque les hace parecer a todos el mismo usuario mediante diversas técnicas llamadas _anti-fingerprint_.

Es recomendable leer los pequeños consejos que dan en la propia página de descarga y el [manual de usuario][tor-browser-manual] para no boicotear tu propia navegación anónima sin querer. Por ejemplo, se desaconseja instalar complementos de navegador.

### Tor Browser en ordenadores

Se puede descargar para Windows, Mac OS o GNU/Linux en su [web oficial](https://www.torproject.org/es/download/ "Descargar Tor Browser en la página del Tor Project"). Si la página está bloqueada, puedes consultar réplicas como las de la [EFF](https://tor.eff.org/) o el [Calyx institute](https://tor.calyxinstitute.org/).  También se puede mandar un correo electrónico a `gettor@torproject.org` o un mensaje directo en Twitter a la cuenta `@get_tor`, escribiendo en el mensaje exclusivamente el nombre de tu plataforma (`windows`, `osx` o `linux`) y, opcionalmente, las siglas del idioma (`en` para inglés, `es` para español, `zh` para chino, etc).

### Tor Browser en móviles

#### Android

Desde la versión 8.5, Tor Browser también está disponible de manera oficial para Android, tras una colaboración técnica de ocho meses con [Guardian Project](https://guardianproject.info/).[^tor-browser-8-5]

Se puede descargar la aplicación de la [web oficial](https://www.torproject.org/es/download/#android "Obtener Navegador Tor para Android en la página del Tor Project"), en Google Play o mediante [F-Droid](https://f-droid.org), activando previamente el repositorio del Guardian Project.

#### iOS (no oficial)

No existe versión oficial del Tor Browser para dispositivos iOS (como iPhone y iPad) por limitaciones que Apple impone en la plataforma y que no permiten ofrecer tanta protección como las versiones de ordenador o Android. Aún con eso, el proyecto recomienda utilizar [Onion Browser](https://onionbrowser.com/),[^tor-browser-iOS] desarrollada por Mike Tigas con ayuda del Guardian Project. Se puede descargar en la Apple Store.

## Instalación manual del servicio en ordenadores

Vamos a suponer que estamos usando alguna distribución reciente basada en Debian.

1. Instalamos el servicio Tor.

    ```
    sudo apt install tor
    ```

2. Activamos el servicio.

    ```
    sudo service tor start
    ```

3. En nuestro navegador, instalamos la extensión [FoxyProxy](https://getfoxyproxy.org/).

    > Nota: la manera oficial recomendada de navegar por la web anónimamente con Tor es utilizar el navegador Tor Browser, que implementa protecciones _anti-fingerprint_.

4. Hacemos clic en el botón de FoxyProxy -> Options. Agregamos un nuevo proxy de tipo SOCKS5, con nombre "Tor", dirección IP 127.0.0.1 y puerto 9050.
5. Hacemos clic de nuevo en este botón y seleccionamos _"Use proxy TOR for all URLs"_.
6. Comprobamos que estamos navegando mediante TOR si podemos acceder, por ejemplo, a DuckDuckGo usando su [dirección onion](https://3g2upl4pq6kufc4m.onion/). Además, la [dirección IP](https://www.iplocation.net/) a través de la cual estaremos saliendo a internet será distinta de la habitual, y podrá estar en cualquier parte del mundo.

TODO: cómo utilizar `torsocks` para enrutar otras conexiones de internet a través de Tor, como, por ejemplo, SSH.

## Ayuda a combatir la censura con tu navegador web

Si tu conexión a internet no está censurada, puedes ayudar a usuarios en países con conexiones a internet censuradas a conectarse a la red Tor. Es tan sencillo como instalar en tu navegador la extensión [Snowflake](https://snowflake.torproject.org) del proyecto Tor:

- Si utilizas Mozilla Firefox, puedes [instalarla desde Mozilla Addons](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/).
- Si utilizas Google Chrome, puedes [instalarla desde Chrome Web Store](https://chrome.google.com/webstore/detail/snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie).

No debes preocuparte de qué páginas web visitan los usuarios a través de tu navegador porque todas las conexiones se realizan a través de Tor, no de tu conexión.

## Referencias

[^tor-animation]: Artículo en el blog del proyecto Tor anunciando el corto vídeo explicativo en varios idiomas: https://blog.torproject.org/releasing-tor-animation

[^tor-browser-8-5]: Artículo en el blog del proyecto Tor anunciando la versión 8.5 del Tor Browser: https://blog.torproject.org/new-release-tor-browser-85

[^tor-browser-iOS]: Artículo en el blog del proyecto Tor sobre Tor Browser en iOS: https://blog.torproject.org/tor-heart-onion-browser-and-more-ios-tor


[tor-browser-manual]: https://tb-manual.torproject.org/es/
