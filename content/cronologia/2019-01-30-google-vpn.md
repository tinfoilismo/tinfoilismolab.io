---
headless: true
resumen: Google VPN
fecha: 2019-01-30
temas:
  - privacidad
sujetos:
  - google
---

Desde 2012, **Google** paga a usuarios para que instalen un VPN o un router en sus casas y analizar así toda su actividad digital (incluyendo la de menores en la familia) saltándose las limitaciones para empresas de Apple. [TechCrunch](https://techcrunch.com/2019/01/30/googles-also-peddling-a-data-collector-through-apples-back-door/)

  - **31 de enero**. Apple reacciona retirándole el certificado para aplicaciones internas a Google. [The Verge](https://www.theverge.com/2019/1/31/18205795/apple-google-blocked-internal-ios-apps-developer-certificate)
  - Sumario: [EFF](https://www.eff.org/deeplinks/2019/02/google-screenwise-unwise-trade-all-your-privacy-cash)
