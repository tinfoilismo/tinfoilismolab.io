---
headless: true
resumen: Sony copyright
fecha: 2019-07-18
temas:
  - algoritmos
  - propiedad intelectual
sujetos:
  - sony
---

**Sony** retira el nuevo vídeo de la banda 65daysofstatic de las plataformas digitales de distribución mediante filtros automatizados que erróneamente reclaman derechos de autor a la banda por sus propias obras. [BoingBoing](https://boingboing.net/2019/07/18/consequence-free-indifference.html)
