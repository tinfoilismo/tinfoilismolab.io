---
headless: true
resumen: Facebook teléfonos
fecha: 2019-03-04
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** utiliza números de teléfono de los usuarios (otra vez) no solo para la verificación en dos pasos para lo que originalmente se pide, sino para que otros usuarios encuentren sus perfiles, y tampoco provee una manera de desactivar este uso. [TechCrunch](https://techcrunch.com/2019/03/03/facebook-phone-number-look-up/) &middot; [EFF](https://www.eff.org/deeplinks/2019/03/facebook-doubles-down-misusing-your-phone-number)
