---
title: "Información útil para desbloquear dispositivos Android"
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/62
etiquetas:
  - eje-apertura
  - eje-privacidad-seguridad
---

Salvo excepciones, los dispositivos Android se pueden desbloquear para instalar sistemas operativos alternativos por motivos de soberanía tecnológica, privacidad o ecología.

Por ejemplo, puedes adaptar mejor los dispositivos que utilizas actualmente a tus usos y costumbres o quitarles [funcionalidades perniciosas]({{< relref capitalismo-de-vigilancia >}}). También puedes aumentar la vida útil más allá del momento en que el fabricante decida parar de actualizar el _software_ de tu dispositivo. O si tienes guardado en algún cajón algún dispositivo perfectamente funcional que ha sufrido la obsolescencia programada del _software_, incluso puedes resucitarlo con alguna nueva función; este proceso se denomina «suprarreciclaje». En palabras de Fairphone, el fabricante de teléfonos de comercio justo, «el móvil más sostenible es aquel que ya tienes».[^fairphone-cita]

Esta técnica la puede aplicar cualquier persona con un mínimo de manejo de ordenadores: solo necesita saber descargar e instalar programas, usar un cable USB, y copiar y pegar comandos en una terminal.

La información necesaria para hacerlo es específica de cada dispositivo y está desperdigada por toda la Web, normalmente en inglés, y con múltiples grados de fidelidad. A continuación, ofrecemos una serie de información útil a la hora de acometer la búsqueda, así como de valorar la posibilidad de desbloquear un dispositivo Android.

## Glosario

Te encontrarás con multitud de términos que se utilizan frecuentemente al describir o guiar los distintos procesos.

**Android**
: Sistema operativo desarrollado por Google, que incluye los servicios móviles de Google (GMS o GApps). También se utiliza para referirse al ecosistema de dispositivos que lo usan y las aplicaciones que funcionan en este sistema.

**AOSP**
: Sistema operativo desarrollador por Google y de código abierto. Es la base de Android y no incluye servicios móviles de Google (al estilo de Chromium/Chrome). Utiliza Linux como núcleo.

**Servicios móviles de Google**, o coloquialmente **GApps**
_Google Mobile Services (GMS)_
: Paquete de aplicaciones y de servicios de Google que se integran en el sistema Android o se exponen para que otras aplicaciones los utilicen. Entre las aplicaciones podemos encontrar la tienda de aplicaciones Play Store, el teclado táctil GBoard o el navegador Chrome; entre los servicios integrados con el sistema, la sincronización de contactos y calendario, y la geolocalización aproximada; y entre los servicios expuestos, la integración de mapas de Google Maps y las notificaciones _push_ (GFM).

**ZIP instalable**
_flashable ZIP_
: Un archivo ZIP preparado para ser instalado mediante un subsistema de recuperación personalizado. Su instalación modifica el _software_ que hay en el dispositivo: puede alterar o reemplazar el sistema operativo, reemplazar el subsistema de recuperación o instalar _firmware_ necesario por el sistema; por ejemplo, [el del módem](https://github.com/WeAreFairphone/modem_zip_generator), el del baúl criptográfico o la imagen _splash_ con la que arranca el dispositivo.

**ROM**
**_firmware_**
: Nombre utilizado para referirnos a un sistema operativo, normalmente Android, específicamente compilado para un dispositivo concreto. Normalmente toma la forma de un archivo ZIP, instalable desde un subsistema de recuperación personalizado, que incluye dentro el árbol de archivos del sistema, y en algunas ocasiones, también _firmware_ para que funcionen diversas partes del dispositivo.

**ROM de fábrica**
_stock ROM_
: ROM con la que viene el teléfono de fábrica. En este caso, normalmente debería incluír no solo el sistema operativo Android, sino también el subsistema de recuperación de fábrica y todos los _firmwares_ necesarios. Es, en general, una garantía de que podrás recuperar tu dispositivo en caso de que algo vaya mal (por ejemplo, si se _brickea_).

**ROM cocinada**
_cooked ROM_
: ROM de fábrica modificada y reempaquetada, con aplicaciones cambiadas, el kernel alterado, distintos temas gráficos o incluso nuevas funcionalidades. La legalidad es dudosa porque se modifica y redistribuye _software_ cubierto por derechos de autor e industriales, incluyendo las GApps, y puede que se requiera su eliminación por parte de las empresas. Son raramente utilizadas hoy día porque la comunidad ha madurado hacia opciones más flexibles como las ROMs personalizadas, que aprovechan el potencial del _software_ libre.

**ROM personalizada**
_custom ROM_
: ROM construída en base al código fuente de AOSP y el _kernel_ Linux del dispositivo, que suele quitar funcionalidades pasajeras de los fabricantes, el _software_ indeseado (_bloatware_) y aquel que recopila información sobre ti (_spyware_, incl. GApps) y añadir funcionalidades extra de personalización, de [control sobre el dispositivo y tus datos](https://blog.virgulilla.com/2018/whatsapp-sin-contactos/), de [copia de seguridad y restauración de aplicaciones](https://calyxinstitute.org/projects/seedvault-encrypted-backup-for-android), etc.. Históricamente, algunas de estas funcionalidades extra han terminado llegando de un modo u otro a las siguientes versiones de Android, aunque a la manera que ha considerado Google.

**Subsistema de recuperación de Android**
_Android recovery_
: Programa de bajo nivel con funciones para restaurar o actualizar el _software_ de un dispositivo Android o resetear al estado de fábrica. Normalmente es invisible para el usuario, pero se puede acceder a él manteniendo pulsadas una combinación de teclas específica al encender el dispositivo. Cuando se utiliza un _recovery_ personalizado, las funciones pueden ser muchas más, como por ejemplo hacer copias de seguridad de tus datos, ver el sistema de archivos o arrancar una terminal de comandos.

**Cargador de arranque de Android** o coloquialmente **modo _fastboot_**
_Android bootloader_, _fastboot mode_
: Programa de dimensiones reducidas que, en los primeros momentos del arranque de un dispositivo, se encarga de cargar el sistema operativo Android. Normalmente es invisible para el usuario, pero se puede parar el proceso de arranque, habitualmente manteniendo pulsadas una combinación de teclas específica al encender el dispositivo.

**Modo de descarga de Samsung**, Loke, o coloquialmente **modo Odin**
: Programa de bajo nivel de los dispositivos Samsung con funciones para restaurar o actualizar el software del dispositivo. Normalmente es invisible para el usuario, pero se puede acceder a él manteniendo pulsados los botones de <kbd>bajar volumen</kbd> e <kbd>inicio</kbd> al encender el dispositivo Samsung.

**ADB**
: Protocolo y herramienta de terminal del SDK de Android para comunicarse con un sistema Android activo, o en ocasiones, con el subsistema de recuperación. Permite acceder a una terminal de comandos, instalar aplicaciones desde un fichero APK, listar aplicaciones, congelarlas (deshabilitarlas) y reiniciar el dispositivo a otros modos, entre otras funcionalidades.

**Fastboot**
: Protocolo y herramienta de terminal del SDK de Android para comunicarse con el cargador de arranque de un dispositivo Android. Permite sobreescribir particiones, extraer alguna información técnica del dispositivo, como su nombre en código, la versión de Android y del _kernel_ de Linux utilizado, la fecha de compilación, etc.. También permite funcionalidades propias del fabricante (OEM), en particular el desbloqueo del cargador de arranque.

**Odin**
: Protocolo y herramienta gráfica de Windows, usada internamente por Samsung, que sirve para comunicarse con el cargador de arranque de los dispositivos Samsung. Existen varias versiones que han sido filtradas y son generalmente inestables. Existe una reimplementación de código abierto llamada [Heimdall](https://www.glassechidna.com.au/heimdall/) que se dio a conocer en [esta publicación en el foro de XDA](https://forum.xda-developers.com/t/loke.747659/post-7552304).

**APK**
: Fichero que representa el paquete de instalación de una aplicación para Android. Incluye el código de la aplicación, un manifiesto de lo que hace y los permisos que requiere, así como la firma criptográfica del autor. Generalmente, los usuarios de Android no las utilizan directamente porque utilizan la aplicación Google Play Store, que descarga los APK e instala las aplicaciones por ellos. Repositorios como [F-Droid](https://f-droid.org/) permiten descargarlas desde su web, además de mediante su aplicación de «tienda».

**Nombre en clave**
_codename_
: Nombre para cada modelo de dispositivo utilizado internamente por los fabricantes en el contexto de Android para referirse a él. Por ejemplo, el Google/LG Nexus 4 lleva el nombre en clave `mako`, mientras que el BQ Aquaris M5 lleva el de `piccolo` o el OnePlus 6, el de `enchilada`.

**_Brickear_** o **dejar como un pisapapeles**
: Estropear un dispositivo, normalmente al manipularlo instalándole _software_ alternativo, de manera que no arranque o no puedan utilizarse sus funciones. El neologismo viene de la expresión «dejar como un ladrillo» en inglés. Se consideran dos niveles de gravedad: el moderado (_soft brick_) y el severo (_hard brick_), que se refieren a la dificultad de restaurar el dispositivo a un estado funcional.

**_Brickeo_ moderado**
_soft brick_
: _Brickeo_ de gravedad moderada, generalmente referido a un fallo de _software_. Se considera que se puede restaurar el dispositivo a un estado funcional mediante la reinstalación de alguna parte del _firmware_. En casos extremos, o cuando se desconoce la causa, puede ser necesario reinstalar la ROM de fábrica, con la consiguiente pérdida de la información.

**_Brickeo_ severo**
_hard brick_
: _Brickeo_ de gravedad severa, generalmente referido a un fallo de _hardware_ o del _firmware_ de bajo nivel. Se considera que no tiene solución en la práctica, bien porque el _hardware_ afectado no es fácilmente reemplazable, o bien porque se requiere un análisis profundo. Acudir al servicio técnico del fabricante puede ser problemático si el dispositivo ha sido desbloqueado, aunque legalmente sigue en garantía.

**Arranque en bucle**
_bootloop_
: Un tipo concreto de _brickeo_ en el que el dispositivo intenta arrancar, y al no poder continuar en algún punto del proceso, se reinicia una y otra vez. Normalmente es un tipo de _brickeo_ moderado que se resuelve detectando el problema concreto (por ejemplo, un sistema intenta arrancar con los datos de usuario de otro sistema operativo sin borrar) o restaurando la ROM de fábrica.


## Consideraciones generales

1. Los dispositivos Android son productos electrónicos embebidos; es decir, diseñados específicamente teniendo en cuenta la limitación de sus recursos como la memoria o la batería. Por esto, la imagen del sistema operativo que corren no es genérica, sino que está específicamente construída para cada uno de los diferentes modelos, incluyendo solo los controladores necesarios; es decir, una imagen de un modelo de teléfono no funciona correctamente en otro. Esto es una diferencia fundamental con los ordenadores de escritorio, en el que la imagen del sistema operativo es genérica y los controladores no forman parte del sistema operativo base.
2. Al desbloquear los dipositivos, es posible que pierdas alguna funcionalidad debido a que [esté limitada por un sistema DRM](https://drm.info/what-is-drm.es.html) ([Defective by design](https://www.defectivebydesign.org/what_is_drm)). Por ejemplo, es sabido que el _firmware_ de postprocesado de imágenes que utilizan los dipositivos de Sony se hace inaccesible al desbloquearlos, así que la cámara, aunque continúa siendo funcional, pierde calidad. También es posible que aplicaciones de reproducción de multimedia que utilizan de manera muy estricta algún tipo de DRM, como Widevine, de Google, se nieguen a funcionar o limiten la calidad. Si el sistema DRM funciona a nivel de aplicación, puede haber maneras de restaurar la funcionalidad, pero no así cuando esté al nivel de _firmware_. Consulta la información disponible sobre tu dispositivo a este respecto antes de desbloquearlo.
3. Al desbloquear o modificar de alguna manera los dispositivos, el fabricante suele amenazar con que pierdes la garantía. Sin embargo, el equipo legal de [la FSFE declara que es una práctica legal en la Unión Europea](https://fsfe.org/activities/upcyclingandroid/is-flashing-legal.html), cubierta por la directiva (UE) 2019/771 de compraventa de bienes.
4. Al desbloquear o modificar de alguna manera los dispositivos, puedes estar abriendo caminos para que otras personas accedan ilegítimamente a tu dispositivo. Considera siempre si lo que estás haciendo podría hacerlo cualquier persona con malas intenciones y cómo te afectaría eso a ti y a las personas que te rodean si extravías o te roban el dispositivo.
5. Todo _software_ que te descargues de fuentes no confiables de internet puede ser un peligro para los datos personales almacenados en tu teléfono, tanto los tuyos como los de tus círculos sociales. Busca siempre los proyectos y fuentes más establecidos o confiables por su trayectoria o pulcritud.
6. Todo _software_ que te descargues de internet carece de garantía. Nadie se responsabilizará si tu dispositivo resulta dañado o malfunciona. Lee detalladamente las instrucciones antes de proceder a usarlo. Si el dispositivo es nuevo, prueba todas las funciones con el _software_ de fábrica antes de desbloquearlo, por si acaso tuvieras que recurrir a la garantía antes. En mi larga experiencia, sin embargo, nunca he tenido ningún problema de este tipo.


## Fuentes de información

Comunitaria:

- [Foro de XDA Developers](https://forum.xda-developers.com/): foro del sitio norteamericano de origen holandés donde se desarrollan ROMs personalizadas. Hay un foro por cada dispositivo. Se pueden encontrar temas que recopilan las ROMs de fábrica (_stock_), guías para desbloquear, para _rootear_ y consejos y conversaciones entorno al mundillo, además de los temas de _add-ons_ como Magisk y Xposed Framework, de los _recoveries_ o las ROMs personalizadas (oficiales o no oficiales de los proyectos), a veces en desarrollo. La moderación es bastante buena y no pasan un tema sin obligar a poner el código fuente.
- [MoDaCo](https://www.modaco.com/): foro británico de desarrollo de ROMs personalizadas y cocina de ROMs. Vivió sus momentos de gloria durante los primeros años de Android, pero no parece estar muy al día. En cualquier caso, había información y desarrollos de muy alta calidad.
- [Foro de HTCManía](https://www.htcmania.com/foro.php): foro del sitio español (madrileño) donde se recopila información y se ayuda, además de cocinar ROMs. La información no suele ser demasiado fiable, pero dado su enfoque territorial, se puede encontrar alguna información particular de modelos o variantes comercializadas en España.

Proyectos:

- [Wiki de LineageOS](https://wiki.lineageos.org/): guías específicas para los dispositivos que soportan o han soportado la ROM personalizada LineageOS (para los que ya no se soportan, marcar (_Show discontinued devices_)). Incluye, por cada dispositivo, combinaciones de teclas para arrancar al modo de recuperación (_recovery_) o al cargador de arranque (_bootloader_), e instrucciones de desbloqueo del cargador de arranque.
- [Preguntas frecuentes de TWRP](https://twrp.me/FAQ/): consejos generales de uso, resolución de preguntas habituales (¿de qué debería hacer una copia de seguridad?), así como terminología utilizada, descripción de procesos, advertencias de seguridad o registro de decisiones tomadas por el proyecto.


## Para profundizar

### Artículos

_¡Libera tu Android!_, una campaña de la FSFE enfocada en retomar el control y la privacidad de los dispositivos que llevan el sistema operativo desarrollado por Google. https://fsfe.org/activities/android/android.es.html

_Upcycling Android_, una campaña de la FSFE enfocada en alargar la vida útil de los dispositivos Android para que no terminen siendo basura electrónica. https://upcyclingandroid.org

_About the Upcycling Android Workshops_, información (en inglés) sobre los talleres de participación abierta en el marco de la campaña Upcycling Android. https://wiki.fsfe.org/Activities/Android/UpcyclingWorkshops/About#About_the_Upcycling_Android_Workshops


## Referencias

[^fairphone-cita]: _The most sustainable phone is the one you already own_, artículo en el blog de Fairphone, 2019. https://www.fairphone.com/2019/05/20/the-most-sustainable-phone-is-the-one-you-already-own/
