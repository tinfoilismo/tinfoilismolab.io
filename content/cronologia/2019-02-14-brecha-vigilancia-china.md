---
headless: true
resumen: Brecha vigilancia estatal China
fecha: 2019-02-14
temas:
  - seguridad
---

Se descubren expuestas al público varias bases de datos de vigilancia estatal en tiempo real de millones de residentes en Xinjiang, China. Algunas incluso muestran restos de haber sido ya accedidas ilícitamente por malos actores. [ZDNet](https://www.zdnet.com/article/chinese-company-leaves-muslim-tracking-facial-recognition-database-exposed-online/) &middot; [EFF](https://www.eff.org/deeplinks/2019/03/massive-database-leak-gives-us-window-chinas-digital-surveillance-state)
