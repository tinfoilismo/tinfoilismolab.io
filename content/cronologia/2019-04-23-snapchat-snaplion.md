---
headless: true
resumen: SnapChat SnapLion
fecha: 2019-04-23
temas:
  - privacidad
sujetos:
  - snapchat
---

Empleados de **SnapChat** abusaron de su acceso privilegiado y accedieron con una herramienta interna, SnapLion, a información sensible de usuarios, como geolocalización, datos y snaps (imágenes y vídeos que desaparecen a las 24 horas). [VICE Motherboard](https://www.vice.com/en_us/article/xwnva7/snapchat-employees-abused-data-access-spy-on-users-snaplion)
