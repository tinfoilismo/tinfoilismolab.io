---
headless: true
resumen: Xsolla despidos AI
fecha: 2021-08-04
temas:
  - algoritmos
  - privacidad
  - transparencia
sujetos:
  - xsolla
---
La proveedora servicios de pago rusa **Xsolla** despide a 150 de sus trabajadores tras analizar su actividad en distintas herramientas, como Jira, Confluence, Gmail, chats, documentos y _dashboards_, mediante el uso de tecnologías de inteligencia artificial. [Game World Observer](https://gameworldobserver.com/2021/08/04/xsolla-fires-150-employees-using-big-data-and-ai-analysis-ceos-letter-causes-controversy) 

- **5 de agosto.** La eficiencia de los puestos de mando no se analizó, y para el cálculo no se consideró el tiempo invertido en algunas herramientas de desarrollo informático como Git o los editores de código. [Game World Observer](https://gameworldobserver.com/2021/08/05/xsolla-cites-growth-rate-slowdown-as-reason-for-layoffs-ceos-tweet-causes-further-controversy)
