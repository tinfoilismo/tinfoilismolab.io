---
headless: true
resumen: Brecha Ecuador
fecha: 2019-10-16
temas:
  - privacidad
  - seguridad
sujetos:
  - novaestrat
---

Se encuentra sin protección una base de datos de **Novaestrat** con información personal actualizada de los ciudadanos de Ecuador, incluyendo 6,7 millones de registros sobre niños. La información filtrada recoge nombre completo, domicilio, relaciones familiares, datos del registro civil, información financiera y matrículas de coches. [vpnMentor](https://www.vpnmentor.com/blog/report-ecuador-leak/) &middot; [ZDNet](https://www.zdnet.com/article/database-leaks-data-on-most-of-ecuadors-citizens-including-6-7-million-children/)
