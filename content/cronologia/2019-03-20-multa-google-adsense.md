---
headless: true
resumen: Multa Google AdSense
fecha: 2019-03-20
temas:
  - sanciones
sujetos:
  - google
---

La Comisión Europea sanciona a **Google** con 1&thinsp;500 millones de € por prácticas monopolísticas (otra vez) referentes a AdSense, su plataforma de publicidad: impidió a los rivales de Google publicar sus anuncios de búsqueda en sitios web de terceros mediante cláusulas restrictivas en los contratos con dichos sitios web. [Comisión Europea](http://europa.eu/rapid/press-release_IP-19-1770_es.htm) &middot; [BBC](https://www.bbc.com/news/business-47639228)
