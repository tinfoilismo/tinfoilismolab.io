---
title: Regulación
summary: Análisis de leyes y regulaciones.
images:
  - articulos/regulacion/icon.svg
colors:
  dark: "#308b60"
  light: "#bdd6cc"
aliases:
  - /categoría/regulación/
---

Análisis de leyes y regulaciones.
