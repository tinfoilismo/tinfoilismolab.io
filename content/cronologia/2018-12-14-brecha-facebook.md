---
headless: true
resumen: Brecha Facebook
fecha: 2018-12-14
temas:
  - seguridad
sujetos:
  - facebook
---

Facebook expuso fotos privadas de hasta 6,8 millones de usuarios a desarrolladores por error. [The Verge](https://www.theverge.com/2018/12/14/18140771/facebook-photo-exposure-leak-bug-millions-users-disclosed)
