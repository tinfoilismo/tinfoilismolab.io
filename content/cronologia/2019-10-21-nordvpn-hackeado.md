---
headless: true
resumen: NordVPN pirateado
fecha: 2019-10-21
temas:
  - privacidad
  - seguridad
  - transparencia
sujetos:
  - nordvpn
---

Un servidor de **un proveedor de NordVPN** fue pirateado en marzo de 2018. NordVPN afirma que el proveedor no avisó de la brecha y que han cerrado el contrato de servicio con ellos. [TechCrunch](https://techcrunch.com/2019/10/21/nordvpn-confirms-it-was-hacked/) &middot; [Comunicado oficial de NordVPN](https://nordvpn.com/blog/official-response-datacenter-breach/) &middot; [Resumen de comunicado en español](https://nordvpn.com/es/blog/acceso-no-autorizado-proveedor/)
