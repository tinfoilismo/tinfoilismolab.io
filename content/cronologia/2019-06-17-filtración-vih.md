---
headless: true
resumen: Filtración VIH
fecha: 2019-06-17
temas:
  - privacidad
---

El sistema público de salud público de Highland, Escocia, manda un correo electrónico sin ocultar los destinatarios. El mensaje era una invitación a un grupo de apoyo para enfermos infectados por <abbr title="virus de la inmunodeficiencia humana">VIH</abbr>. [BBC](https://www.bbc.com/news/uk-scotland-highlands-islands-48662386)
