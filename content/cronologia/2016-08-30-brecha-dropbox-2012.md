---
headless: true
resumen: Brecha Dropbox 2012
fecha: 2016-08-30
temas:
  - seguridad
  - transparencia
sujetos:
  - dropbox
---

En la brecha de 2012 de **Dropbox** se filtraron contraseñas y direcciones de correo de más de 68 millones de usuarios, dos tercios de su base de usuarios. En el momento, Dropbox solo comunicó que se habían filtrado direcciones de correo de usuarios, pero no contraseñas. [VICE Motherboard](https://www.vice.com/en_us/article/nz74qb/hackers-stole-over-60-million-dropbox-accounts) &middot; [The Guardian](https://www.theguardian.com/technology/2016/aug/31/dropbox-hack-passwords-68m-data-breach)

  - **31 de julio de 2012.** Comunicación oficial de la filtración. [Blog de Dropbox](https://blogs.dropbox.com/dropbox/2012/07/security-update-new-features/)
