---
headless: true
resumen: Facebook Bach
fecha: 2019-09-04
temas:
  - algoritmos
  - propiedad intelectual
sujetos:
  - facebook
  - sony
---

**Facebook** silencia una interpretación de Bach del pianista James Rhodes por detectar que 47 segundos pertenecen supuestamente a Sony Music Global. [Twitter de James Rhodes](https://twitter.com/JRhodesPianist/status/1036929244654460928) &middot; [BoingBoing](https://boingboing.net/2018/09/05/mozart-bach-sorta-mach.html)
