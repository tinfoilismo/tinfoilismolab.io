---
headless: true
resumen: Vulnerabilidad GSuite
fecha: 2019-05-21
temas:
  - seguridad
sujetos:
  - google
---

Google soluciona un error por el que, desde 2005, se almacenaron las contraseñas de los usuarios empresariales de G Suite en texto plano. [BleepingComputer](https://www.bleepingcomputer.com/news/security/google-stored-unhashed-g-suite-passwords-for-over-a-decade/)
