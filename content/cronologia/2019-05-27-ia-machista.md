---
headless: true
resumen: IA machista
fecha: 2019-05-27
temas:
  - privacidad
  - algoritmos
  - machismo
---

Un programador chino afincado en Alemania presuntamente diseñó un sistema de reconocimiento facial que identifica a las actrices de vídeos de sitios web pornográficos con sus perfiles sociales personales. La herramienta se creó con el objetivo de que los usuarios comprobasen si sus novias habían participado alguna vez en la pornografía. [YiquinFu en Twitter](https://twitter.com/yiqinfu/status/1133215940936650754)

  - El programador se disculpa y promete haber borrado toda la información. [TechnologyReview](https://www.technologyreview.com/s/613607/facial-recognition-porn-database-privacy-gdpr-data-collection-policy/)
